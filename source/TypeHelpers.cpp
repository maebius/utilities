#include "TypeHelpers.h"

#include <cstdio>
#include <map>
#include <sstream>
#include <iomanip>
#include <ctime>

#include "Debug.h"

namespace TypeHelpers
{
	using namespace std;

	std::string getSuffix(int number)
	{
		const map<int, std::string> suffixes
		{
			{1, "st" }
			, {2, "nd" }
			, {3, "rd" }
		};
	
		auto ite = suffixes.find(number);
		if (ite != suffixes.end())
		{
			return ite->second;
		}
		return string("th");
	}

	time_t getCurrentTime()
	{
		return time(nullptr);
	}

	time_t getTime(string timeString, string format)
	{
		istringstream ss(timeString);
		struct tm tm;
		ss >> get_time(&tm, format.c_str());
		if (ss.fail()) 
		{
			ERROR_PRINT("Failed parsing time from '%s' for format: '%s'",  timeString.c_str(), format.c_str());
		}
		return mktime(&tm);
	}			

	string getTime(time_t time, string format)
	{
		auto tm = *localtime(&time);
		std::ostringstream oss;
		oss << put_time(&tm, format.c_str());
		return oss.str();
	}


} // namespace TypeHelpers

	
