
#include <vector>
#include <map>
#include <cassert>
#include <fstream>
#include <algorithm>
#include <iostream>

#include "GraphUtilities.h"
#include "RandomHelpers.h"
#include "PrintHelpers.h"

namespace graph_utilities
{
	using namespace Eigen;
	using namespace random_helpers;

	Graph::~Graph()
	{
		for (auto e : edges)
		{
			delete e;
		}
		for (auto n : nodes)
		{
			delete n;
		}
	}

	std::vector<Edge*> Graph::getOutgoingEdges(const Node* node)
	{
		std::vector<Edge*> result;

		for (auto e : edges)
		{
			if (e->source == node)
			{
				result.push_back(e);
			}
		}

		return result;
	}

	Edge* Graph::getEdge(const Node* src, const Node* trg)
	{
		for (auto e : edges)
		{
			if (e->source == src && e->destination == trg)
			{
				return e;
			}
		}
		return nullptr;
	}

	Record* getMostPromising(Records& records)
	{
		assert(!records.empty());
			
		// take most promising node
		Record* result = records[0];
		for (int i = 1; i < records.size(); i++)
		{
			if (records[i]->estimatedTotalCost < result->estimatedTotalCost)
			{
				result = records[i];
			}
		}
		return result;
	}

	Record* getMostPromising(OrderedRecords& records)
	{
		assert (!records.empty());
		
		return records.top();
	}

	RecIte getRecord(Node* node, Records& records)
	{
		return std::find_if(records.begin(), records.end()
			, [&](const Record* r) { return node == r->node; });
	}

	RecIte getRecord(Node* node, OrderedRecords & records)
	{
		return std::find_if(records.getData().begin(), records.getData().end()
			, [&](const Record* r) { return node == r->node; });
	}


	void createRandomPositionsInCube(
		const int count
		, const vec3& boundingOrigin
		, const vec3& halfDimension
		, std::vector<vec3>& outPositions)
	{
		// first just create the disconnected nodes
		for (int i = 0; i < count; i++)
		{
			auto r = vec3::Random(); // returns a vector in where components are [-1,1]
			outPositions.push_back(boundingOrigin + r.cwiseProduct(halfDimension));
		}	
	}

	void createRandomPositionsInGrid2D(
		const int count
		, const vec3& bottomLeft
		, const vec3& bottomRight
		, const vec3& topLeft
		, const vec3& topRight
		, std::vector<vec3>& outPositions)
	{
		// 1st, create grid / vertex positions 

		// treat the area as it would be a rectangle, formed by two vectors
		// -> divide the area formed by that for the cell count, and take the axis division increments for that

		vec3 bottom = bottomRight - bottomLeft;
		vec3 left = topLeft - bottomLeft;

		float bottomWidth = bottom.norm();
		float leftHeight = left.norm();

		bottom.normalize();
		left.normalize();

		vec3 top = topRight - topLeft;
		vec3 right = topRight - bottomRight;

		float topWidth = top.norm();
		float rightHeight = right.norm();

		top.normalize();
		right.normalize();

		float cellBottomLength = bottomWidth / sqrt(count);
		float cellLeftHeight = leftHeight / bottomWidth * cellBottomLength;

		int cellWidthCount = ceil(bottomWidth / cellBottomLength);
		int cellHeightCount = ceil(leftHeight / cellLeftHeight);

		float cellRightHeight = rightHeight / cellHeightCount;

		for (int row = 0; row < cellHeightCount; row++)
		{
			vec3 leftPos = bottomLeft + cellLeftHeight * row * left;
			vec3 rightPos = bottomRight + cellRightHeight * row * right;

			vec3 hAxis = rightPos - leftPos;
			float hNorm = hAxis.norm();
			hAxis.normalize();

			float cellWidth = hNorm / cellWidthCount;

			for (int col = 0; col < cellWidthCount; col++)
			{
				vec3 pos = leftPos + col * cellWidth * hAxis;   

				outPositions.push_back(pos);
				
				if (outPositions.size() >= count)
					return;
			}
		}
	}

	Graph* createRandomGraph(
		const int positionCount
		, const int nodeCount
		, const vec3& boundingOrigin
		, const vec3& halfDimension
		, const bool biDirectional)
	{
		// this is awfully slow ...
		// TODO: put all created nodes to kd-tree
		// - then take a range between some distance of other nodes
		// - ... and make connections to some random amount


		// TODO: Edge's and Node's created here are deleted nowhere!!!

		assert(nodeCount <= positionCount);

		std::vector<vec3> positions;

		vec3 bottomLeft { -5.0f, -3.0f, 0.0f };
		vec3 bottomRight { 5.0f, -5.0f, 0.0f };
		vec3 topLeft { -2.0f, 5.0f, 0.0f };
		vec3 topRight { 2.0f, 5.0f, 0.0f };
		createRandomPositionsInGrid2D(
			positionCount
			, bottomLeft
			, bottomRight
			, topLeft
			, topRight
			, positions);
		//createRandomPositionsInCube(positionCount, boundingOrigin, halfDimension, positions);
		
		std::vector<Node*> sortingVec;
		std::map<Node*, int> connectionCounts;
		
		Graph* graph = new Graph();

		for (int i = 0; i < nodeCount; i++)
		{
			int index = random_helpers::getRandom(0, positions.size()-1);
			auto ite = positions.begin();
			ite += index;
			vec3 selection = *ite; 
			positions.erase(ite);

			auto node = new Node{ selection };

			graph->nodes.push_back(node);
			sortingVec.push_back(node);
		}
	
		// then for each node, create some amount of edges
		for (int i = 0; i < nodeCount; i++)
		{
			auto currentNode = graph->nodes[i];
			auto pos = currentNode->position;
			// based on our current node, order all other based on increasing distance
			std::sort(
				sortingVec.begin()
				, sortingVec.end()
				, [&pos](Node* first, Node* second) 
					{
						return (first->position - pos).squaredNorm() < (second->position - pos).squaredNorm();
					}
			);
			
			const int minEdgesPerNode = 1;
			const int maxEdgesPerNode = 4;

			// now, add a small random amount of edges for the node, starting from the closest one
			// need to keep separately count for max, as edges are thought two way, so other nodes can add edges to each node as well
			int edges = getRandom(minEdgesPerNode, maxEdgesPerNode);
			for (int j = 1; j <= edges && connectionCounts[currentNode] < maxEdgesPerNode; j++)
			{
				auto otherNode = sortingVec[j];

				auto cost = (pos - otherNode->position).squaredNorm();
				auto edge = new Edge{ currentNode, otherNode, cost };				
				
				graph->edges.push_back(edge);
				connectionCounts[currentNode]++;

				if (biDirectional)
				{
					auto otherEdge = new Edge{ otherNode, currentNode, cost };
					graph->edges.push_back(otherEdge);
					connectionCounts[otherNode]++;
				}
			}
		}
		return graph;
	}

	Graph* loadTriangleGraphFromFile(const std::string& filename)
	{
		Graph* graph = new Graph();

	 	std::vector<std::string> lines;

        std::fstream fin(filename, std::fstream::in);
        std::string line;
		vec3 zero{0.f, 0.f, 0.f};

		std::vector<Node*> previousLine;
		std::vector<Node*> currentLine;

        while (std::getline(fin, line))
        {
			std::istringstream l(line);
			std::string s;
			
			while(getline(l, s, ' '))
			{
				int v = std::stoi(s);
				auto node = new Node{ zero, v };
				currentLine.push_back(node);
				graph->nodes.push_back(node);
			}

			if (previousLine.size() > 0)
			{
				for (int i = 0; i < previousLine.size(); i++)
				{
					auto edge = new Edge{ previousLine[i], currentLine[i], 0 };				
					auto edge2 = new Edge{ previousLine[i], currentLine[i+1], 0 };

					graph->edges.push_back(edge);
					graph->edges.push_back(edge2);
				}
			}
			previousLine.clear();
			std::copy(currentLine.begin(), currentLine.end(), std::back_inserter(previousLine));
			currentLine.clear();
        }  	
	
		return graph; 
	}
}
