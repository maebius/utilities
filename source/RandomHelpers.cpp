#include "RandomHelpers.h"

#include <random>

namespace random_helpers
{
	using namespace std;

	static std::random_device rd;
	static std::mt19937 gen(rd());

	int getRandom(int min, int max)
	{
		std::uniform_int_distribution<> dis(min, max);
		return dis(gen);
	}

	double getRandom(double min, double max)
	{
		uniform_real_distribution<> dis(min, max);
		return dis(gen);
	}

	void fill(int min, int max, int count, std::vector<long long>& result)
	{
		for (int i = 0; i < count; i++)
		{
			result.push_back(getRandom(min, max));
		}
	}
}

