
#include "sortingException.h"
#include "Debug.h"

namespace sort_utilities
{
	SortingException::SortingException(std::string error)
	{
		ERROR_PRINT("sort_utilities exception caught: %s", error.c_str());
	}
};

