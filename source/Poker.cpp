#include "Poker.h"

#include <string>
#include <vector>
#include <iterator>
#include <cassert>
#include <algorithm>
#include <iostream>

using namespace std;

namespace poker
{
	const std::map<HandValue, std::string> HAND_VALUE_TO_STRING = 
	{
		{ HandValue::HighCard, "HighCard" }
		, { HandValue::Pair, "Pair" }
		, { HandValue::TwoPairs, "TwoPairs" }
		, { HandValue::ThreeOfAKind, "ThreeOfAKind" }
		, { HandValue::Straight, "Straight" }
		, { HandValue::Flush, "Flush" }
		, { HandValue::FullHouse, "FullHouse" }
		, { HandValue::FourOfAKind, "FourOfAKind" }
		, { HandValue::StraightFlush, "StraightFlush" }
	}; 


	ostream& operator<<(ostream& strm, const Card& card)
	{
		switch (card.value)
		{
			case 10:
				strm << "T";
				break;
			case 11:
				strm << "J";
				break;
			case 12:
				strm << "Q";
				break;
			case 13:
				strm << "K";
				break;
			case 14:
				strm << "A";
				break;
			default:
				strm << card.value;
				break;
		}
		strm << (unsigned char) card.suit;
		return strm;
	}

	Hand::Hand(const std::string v)
	{
		parse(v);
		evaluate();
	}

	void Hand::parse(const std::string v)
	{
		assert(v.size() == 14);

		istringstream iss(v);
		vector<string> tokens
		{
			istream_iterator<string>{iss}
			, istream_iterator<string>{}
		};

		assert(tokens.size() == 5);

		for (int i = 0; i < tokens.size(); i++)
		{
			Suit suit;
			switch (tokens[i][1])
			{
				case 'H':
					suit = Suit::Heart;
					break;
				case 'D':
					suit = Suit::Diamond;
					break;
				case 'S':
					suit = Suit::Spade;
					break;
				case 'C':
					suit = Suit::Club;
					break;
				default:
					assert(false);
			}
			short value = 0;
			switch (tokens[i][0])
			{
				case 'T':
					value = 10;
					break;
				case 'J':
					value = 11;
					break;
				case 'Q':
					value = 12;
					break;
				case 'K':
					value = 13;
					break;
				case 'A':
					value = 14;
					break;
				default:
					value = atoi(&tokens[i][0]);
					break;
			}
		
			_cards.push_back(Card{ suit, value });
		}
	}

	void Hand::evaluate()
	{
		evaluateCategory();
		evaluateInsideCategory();
	}
	
	void Hand::evaluateCategory()
	{
		std::sort(_cards.begin(), _cards.end()
			, [](Card& a, Card& b)
			{
				return a.value > b.value;
			});

		short sameValueCount	= 1;	
		bool isFlush			= true;
		bool isStraight			= true;
		
		for (int i = 1; i < _cards.size(); i++)
		{
			bool sameAsLast = _cards[i].value == _cards[i-1].value;

			if (sameAsLast)
			{
				sameValueCount++;
			}

			if (_cards[i].value != _cards[i-1].value - 1
				&& !(_cards[i-1].value - 1 == 14 && _cards[i].value == 5))
			{
				isStraight = false;
			}

			if (_cards[i].suit != _cards[i-1].suit)
			{
				isFlush = false;
			}

			if (!sameAsLast || i == _cards.size() - 1)
			{
				if (sameValueCount > 1)
				{
					_sameSequences.push_back(sameValueCount);
					_sameSequenceValues.push_back(_cards[i-1].value);
				}
				sameValueCount = 1;
			}
		}

		if (isStraight && isFlush)
		{
			_highLevelValue = HandValue::StraightFlush;
		}
		else if (isStraight)
		{
			_highLevelValue = HandValue::Straight;
		}
		else if (isFlush)
		{
			_highLevelValue = HandValue::Flush;
		}
		else if (_sameSequences.size() > 0)
		{
			assert (_sameSequences.size() < 3);

			if (_sameSequences.size() == 1)
			{
				switch (_sameSequences[0])
				{
					case 2:
						_highLevelValue = HandValue::Pair;
						break;
					case 3:
						_highLevelValue = HandValue::ThreeOfAKind;
						break;
					case 4:
						_highLevelValue = HandValue::FourOfAKind;
						break;
				}
			}
			else
			{
				if (_sameSequences[0] == 2 && _sameSequences[1] == 2)
				{
					_highLevelValue = HandValue::TwoPairs;
				}
				else
				{
					_highLevelValue = HandValue::FullHouse;
				}
			}
		}
		else
		{
			_highLevelValue = HandValue::HighCard;
		}
	}
	
	void Hand::moveSameAsToPosition(int from, int to, short value)
	{
		auto ite = std::find_if(
					_cards.begin() + from
					, _cards.end()
					, [&](const Card & c) 
					{
						return c.value == value;
					});
		
		//cout << "start: " << from << ", found: " << *ite << endl;

		Card newCard(*ite);
		_cards.erase(ite);
		_cards.insert(_cards.begin() + to, newCard);
	} 

	void Hand::moveDifferentThanToPosition(int from, int to, short value)
	{
		auto ite = std::find_if(
					_cards.begin() + from
					, _cards.end()
					, [&](const Card & c) 
					{
						return c.value != value;
					});
				
		Card newCard(*ite);
		_cards.erase(ite);
		_cards.insert(_cards.begin() + to, newCard);
	} 

	void Hand::reorderHand()
	{
		switch (_highLevelValue)
		{
			case HandValue::Pair:
			{
				//cout << "before: " << *this << endl;
				moveSameAsToPosition(0, 0, _sameSequenceValues[0]);
				moveSameAsToPosition(1, 1, _sameSequenceValues[0]);
				//cout << "after: " << *this << endl;
				break;
			}
			case HandValue::TwoPairs:
			{
				int first = (_sameSequenceValues[0] > _sameSequenceValues[1])
					? 0 : 1;
				int second = (first == 0) ? 1 : 0;

				//cout << "before: " << *this << endl;
				moveSameAsToPosition(0, 0, _sameSequenceValues[first]);
				moveSameAsToPosition(1, 1, _sameSequenceValues[first]);
				moveSameAsToPosition(2, 2, _sameSequenceValues[second]);
				moveSameAsToPosition(3, 3, _sameSequenceValues[second]);
				//cout << "after: " << *this << endl;
				break;
			}
			case HandValue::ThreeOfAKind:
			{
				//cout << "before: " << *this << endl;
				moveSameAsToPosition(0, 0, _sameSequenceValues[0]);
				moveSameAsToPosition(1, 1, _sameSequenceValues[0]);
				moveSameAsToPosition(2, 2, _sameSequenceValues[0]);
				//cout << "after: " << *this << endl;
				break;
			}
			case HandValue::FullHouse:
			{
				int first = (_sameSequences[0] > _sameSequences[1])
					? 0 : 1;

				//cout << "before: " << *this << endl;
				moveSameAsToPosition(0, 0, _sameSequenceValues[first]);
				moveSameAsToPosition(1, 1, _sameSequenceValues[first]);
				moveSameAsToPosition(2, 2, _sameSequenceValues[first]);
				//cout << "after: " << *this << endl;
				break;
			}
			case HandValue::FourOfAKind:
			{
				//cout << "before: " << *this << endl;
				moveDifferentThanToPosition(0, 4, _sameSequenceValues[0]);
				//cout << "after: " << *this << endl;
				break;
			}
			default:
				// high card, straight, flush, and straight flush 
				// are already in order (as sorted by highest initially)
				break;
		}
	}

	void Hand::evaluateInsideCategory()
	{
		reorderHand();

		int ce = 1;
		int sum = 0;
		for_each(_cards.rbegin()
			, _cards.rend()
			, [&sum, &ce](const Card& c)
			{
				sum += ce*c.value;
				ce *= 14;
			});
		_detailedValue = sum;
	}

	bool operator <(const Hand& lhs, const Hand& rhs)
	{
		if (lhs._highLevelValue == rhs._highLevelValue)
		{
			return lhs._detailedValue < rhs._detailedValue;
		}

		return lhs._highLevelValue < rhs._highLevelValue;
	}

	bool operator >(const Hand& lhs, const Hand& rhs)
	{
		return rhs < lhs;
	}

	bool operator <=(const Hand& lhs, const Hand& rhs)
	{
		return !(rhs > lhs);
	}

	bool operator >=(const Hand& lhs, const Hand& rhs)
	{
		return !(lhs < rhs);
	}

	bool operator ==(const Hand& lhs, const Hand& rhs)
	{
		return lhs._highLevelValue == rhs._highLevelValue
			&& lhs._detailedValue == rhs._detailedValue;

	}

	bool operator !=(const Hand& lhs, const Hand& rhs)
	{
		return !(lhs == rhs);
	}

	ostream& operator<<(ostream& strm, const Hand& hand)
	{
		if (hand._cards.size() > 0)
		{
			for (int i = 0; i < hand._cards.size()-1; i++)
			{
				strm << hand._cards[i] << " ";
			}
			strm << hand._cards[hand._cards.size()-1];

			strm << " [" << HAND_VALUE_TO_STRING.at(hand._highLevelValue) << "]";
		}
		return strm;
	}
}