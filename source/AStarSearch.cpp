
#include <vector>
#include <map>
#include <functional>
#include <memory>
#include <algorithm>

#include "VectorDef.h"
#include "RandomHelpers.h"

#include "AStarSearch.h"
#include "priorityQueue.h"

#define USE_PRIORITY_QUEUE

namespace graph_search
{
	using namespace Eigen;
	using namespace random_helpers;
	using namespace graph_utilities;

	auto RecordPointerComparer = [](const Record* lhs, const Record* rhs) { return *lhs > *rhs; };

	struct AStarSearch::Impl	
	{
		Graph*		graph;
		Heurestic	heurestic;

		Node* 		start;
		Goals 		goals;

		Record* 	current;

#ifdef USE_PRIORITY_QUEUE
		OrderedRecords		openList{nullptr, RecordPointerComparer};
#else
		Records 			openList;
#endif

		Records 			closedList;

		SearchState			state;

		std::vector<Edge*>	result;

		void cleanLists()
		{
			for (auto r : openList)
			{
				delete r;
			}

			for (auto r : closedList)
			{
				delete r;
			}

			openList.clear();
			closedList.clear();
			result.clear();
		}
	};


	AStarSearch::AStarSearch(graph_utilities::Graph* graph, graph_utilities::Heurestic h)
		: _pimpl(std::make_unique<Impl>())
	{
		_pimpl->graph = graph;
		_pimpl->heurestic = h;
		_pimpl->start = nullptr;
		_pimpl->state = SearchState::UnInitialised;
		_pimpl->current = nullptr;
	}
	
	AStarSearch::~AStarSearch()
	{
		_pimpl->cleanLists();
	}

	void AStarSearch::initializeSearch(Node* start, Goals goals)
	{
		_pimpl->cleanLists();

		// our result path, ends up being empty if no path from start to goal

		_pimpl->start = start;
		for (auto g : goals)
		{
			_pimpl->goals.push_back(g);
		}
		_pimpl->state = SearchState::Running;

		// make a starting record and put it as the seed element in open list
		auto startRecord = new Record{ start, nullptr, 0.0f, 0.0f };
		startRecord->estimatedTotalCost = getLowestHeuresticCost(start);
		
		_pimpl->openList.push_back(startRecord);
	}

	SearchState AStarSearch::getState()
	{
		return _pimpl->state;
	}

	std::vector<Edge*> AStarSearch::getResult()
	{
		// NOTE! this also returns currently unfinished paths

		auto current = _pimpl->current;
		_pimpl->result.clear();

		// go back as long as we are not in the start node
		while (current->node != _pimpl->start)
		{
			_pimpl->result.push_back(current->edge);
			current = *getRecord(current->edge->source, _pimpl->closedList);
		}
		// reverse, as we want it in first to last order
		std::reverse(_pimpl->result.begin(), _pimpl->result.end());

		return _pimpl->result;
	}

	bool AStarSearch::isGoalNode(const Node* node)
	{
		return std::find(_pimpl->goals.begin(), _pimpl->goals.end(), node) != _pimpl->goals.end();
	}

	float AStarSearch::getLowestHeuresticCost(const Node* endNode)
	{
		float heuresticCost = 0.0f;
		if (_pimpl->heurestic != nullptr)
		{
			heuresticCost = std::numeric_limits<float>::max();
			for (auto g : _pimpl->goals)
			{
				auto cost = _pimpl->heurestic(_pimpl->graph, endNode, g);
				heuresticCost = fmin(heuresticCost, cost);
			}
		}
		return heuresticCost;
	}

	bool AStarSearch::execute(int nodesPerIteration)
	{
		auto processed = 0;

		// from the open (= non handled node) list, take the most promising candinate node and expand it
		while (_pimpl->openList.size() > 0)
		{
			_pimpl->current = getMostPromising(_pimpl->openList);
			
			auto current = _pimpl->current;

			// if this is already a goal, we can just terminate
			if (isGoalNode(current->node))
			{
				break;
			}
			
			// handle all outgoing edges
			auto edges = _pimpl->graph->getOutgoingEdges(current->node);
			for (auto edge : edges)
			{
				// calculate the cost to the node from this edge
				auto endNode = edge->destination;
				auto cost = current->costSoFar + edge->cost;

				float endNodeHeurestic = 0.0f;
				Record* endRecord = nullptr;
				
				// has this end node already been handled (in closed list)?
				auto ite = getRecord(endNode, _pimpl->closedList);
				if (ite != _pimpl->closedList.end())
				{
					// yes, so we need to check if this route is shorter than the previous 
					endRecord = *ite;
					if (endRecord->costSoFar <= cost)
					{
						// no, the stored one was shorter, skip this edge
						continue;
					}
					// this is shorter than the stored so we need to reopen
					_pimpl->closedList.erase(ite);

					// use node's old costs to calculate the heurestic cost
					// (avoids call to heurestic function)
					endNodeHeurestic = endRecord->estimatedTotalCost - endRecord->costSoFar;
				}
				else
				{
					// is this node in the open list?
					auto ite = getRecord(endNode, _pimpl->openList);
					if (ite != _pimpl->openList.end())
					{
						// yes, we need to check if this route is shorter one
						endRecord = *ite;
						if (endRecord->costSoFar <= cost)
						{
							// no, the stored one was shorter, skip this edge
							continue;
						}

						// use node's old costs to calculate the heurestic cost
						// (avoids call to heurestic function)
						endNodeHeurestic = endRecord->estimatedTotalCost - endRecord->costSoFar;
					}
					else
					{
						// this is unvisited node, make a record out of it
						endRecord = new Record{ endNode, nullptr, 0.0f, 0.0f };
						endNodeHeurestic = getLowestHeuresticCost(endNode);
					}
				}

				// if here, means this edge was the promising so far
				// and we need to update the record
				endRecord->costSoFar = cost;
				endRecord->edge = edge;
				endRecord->estimatedTotalCost = cost + endNodeHeurestic;

				// now add to open list if not already there
				if (getRecord(endNode, _pimpl->openList) == _pimpl->openList.end())
				{
					_pimpl->openList.push_back(endRecord);
				}
			}

			// all edges handled for current open list element, so marking that as closed
			auto ite = getRecord(current->node, _pimpl->openList);
			_pimpl->openList.erase(ite);
			_pimpl->closedList.push_back(current);

			processed++;

			// do we want to pause execution? if so, just call execute again ...
			if (nodesPerIteration > 0 && processed == nodesPerIteration)
			{
				return false;
			}
		}

		// coming here means the seach has finished
		_pimpl->state = isGoalNode(_pimpl->current->node)
			? SearchState::Success
			: SearchState::Fail;

		return true;
	}
}