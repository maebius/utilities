#include "Triangle.h"
#include <Eigen/Dense>

#include <algorithm>


Triangle::Triangle(vec3 p1, vec3 p2, vec3 p3)
: p{p1, p2, p3}
{
	centroid = triangle_helpers::calculateCentroid(&p1, &p2, &p3);
}

namespace triangle_helpers
{
	vec3 calculateCentroid(vec3* a, vec3* b, vec3* c)
	{
		return (*a + *b + *c) / 3;
	}

	double projectToLine(const vec3& a, const vec3& b, const vec3& p)
	{
		auto ab = b - a;
		auto ap = p - a;
		return ap.dot(ab) / ab.dot(ab);
	}

	vec3 getClosestToSegment(const vec3& a, const vec3& b, const vec3& p)
	{
		auto ce = projectToLine(a, b, p);
		ce = std::max(ce, 0.0);
		ce = std::min(1.0, ce);
		return a + ce * (b - a);
	}

	// adapted from: http://www.blackpawn.com/texts/pointinpoly/
	vec3 getClosestPoint(Triangle* triangle, const vec3& point)
	{		
		// vectors defining the triangle plane
		auto v0 = triangle->p[1] - triangle->p[0];
		auto v1 = triangle->p[2] - triangle->p[0];

		// plane normal, normalized
		auto n = v0.cross(v1).normalized();

		// from point to plane
		auto p = point - triangle->p[0];

		// projection to plane
		auto proj = point - p.dot(n) * n;

		// vector from proj to our point of interest
		auto v2 = proj - triangle->p[0];

		// use barycentric coordinates to check if point is in triangle
		auto d00 = v0.dot(v0);
		auto d01 = v0.dot(v1);
		auto d02 = v0.dot(v2);
		auto d11 = v1.dot(v1);
		auto d12 = v1.dot(v2);

		auto invDenom = 1.0f / (d00 * d11 - d01 * d01);
		auto u = (d11 * d02 - d01 * d12) * invDenom;
		auto v = (d00 * d12 - d01 * d02) * invDenom;

		auto uOk = (u >= 0);
		auto vOk = (v >= 0);
		auto sumOk = (u + v < 1);

		if (uOk && vOk && sumOk)
		{
			// the point is inside, or on the edge
			return triangle->p[0] + u * v0 + v * v1;
		}

		// point outside triangle

		// figure out what corner / edge is closest
		// -> the area outside the triangle is divided in 6 (think of continuing the edges to ad inifitum)

		// we check 3 scenarios
		auto s1 = getClosestToSegment(triangle->p[0], triangle->p[1], point);
		auto s2 = getClosestToSegment(triangle->p[0], triangle->p[2], point);
		auto s3 = getClosestToSegment(triangle->p[1], triangle->p[2], point);

		auto s1l = (point - s1).squaredNorm();
		auto s2l = (point - s2).squaredNorm();
		auto s3l = (point - s3).squaredNorm();

		if (s1l < s2l)
		{
			return (s1l < s3l) ? s1 : s3;
		}
		else
		{
			return (s2l < s3l) ? s2 : s3;
		}
	}
};