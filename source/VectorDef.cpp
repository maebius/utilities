#include "VectorDef.h"

vec3 vec4_to_vec3(const vec4 & src)
{
    return vec3{ src[0], src[1], src[2] };
}

vec4 vec3_to_vec4(const vec3 & src, float fourth)
{
    return vec4{ src[0], src[1], src[2], fourth };
}