
#include "LearningRealTimeAStarSearch.h"
#include <map>
#include <numeric>

using namespace graph_search;

using namespace Eigen;
using namespace graph_utilities;

struct LearningRealTimeAStarSearch::Impl	
	{
		Graph*		graph;
		Heurestic	heurestic;

		Node* 		start;
		Goals 		goals;

        Node*       previous;
		Node*   	current;

        std::map<Node*, float> costEstimates;

		SearchState			state;

        using Action = std::pair<Node*, Edge*>;
        std::map<Action, Node*> result;
		//std::vector<Edge*>	result;

		void cleanLists()
		{
			costEstimates.clear();
			result.clear();
		}
	};

LearningRealTimeAStarSearch::LearningRealTimeAStarSearch(Graph* graph, Heurestic h)
    : _pimpl(std::make_unique<Impl>())
{
    _pimpl->graph = graph;
	_pimpl->heurestic = h;
	_pimpl->start = nullptr;
	_pimpl->state = SearchState::UnInitialised;
    _pimpl->previous = nullptr;
	_pimpl->current = nullptr;
}

LearningRealTimeAStarSearch::~LearningRealTimeAStarSearch()
{
    _pimpl->cleanLists();
}

void LearningRealTimeAStarSearch::initializeSearch(Node* start, Goals goals)
{
    _pimpl->start = start;
    for (auto g : goals)
    {
        _pimpl->goals.push_back(g);
    }	
	_pimpl->state = SearchState::Running;
}

bool LearningRealTimeAStarSearch::isGoalNode(const Node* node)
{
    return std::find(_pimpl->goals.begin(), _pimpl->goals.end(), node) != _pimpl->goals.end();
}

float LearningRealTimeAStarSearch::getLowestHeuresticCost(const Node* endNode)
{
    float heuresticCost = 0.0f;
    if (_pimpl->heurestic != nullptr)
    {
        heuresticCost = std::numeric_limits<float>::max();
        for (auto g : _pimpl->goals)
        {
            auto cost = _pimpl->heurestic(_pimpl->graph, endNode, g);
            heuresticCost = fmin(heuresticCost, cost);
        }
    }
    return heuresticCost;
}

bool LearningRealTimeAStarSearch::execute(int nodesPerIteration)
{
    // from AI A Modern Approach, page 152

/*
    if (_pimpl->state != SearchState::Running)
    {
        return false;
    }
    if (isGoalNode(_pimpl->current))
    {
        _pimpl->state = SearchState::Success;
        return true;
    }
    if (_pimpl->costEstimates.find(_pimpl->current) == _pimpl->costEstimates.end())
    {
        _pimpl->costEstimates[_pimpl->current] = getLowestHeuresticCost(_pimpl->current);
    }
    if (_pimpl->previous)
    {
        Edge* e = _pimpl->graph->getEdge(_pimpl->previous, _pimpl->current);
        _pimpl->result[Action{ _pimpl->previous, e }] = _pimpl->current;

        float smallestCost = numeric_limits<float>::max();

        auto edges = _pimpl->graph->getOutgoingEdges(current->node);
		for (auto edge : edges)
		{
            float cost = numeric_limits<float>::max();

            // TOIMIIKO? pitais vissiin kattoon pairin memberit
            auto t = _pimpl->result.find(Action{_pimpl->previous, edge});
            if (t == _pimpl->result.end())
            {
                cost = getLowestHeuresticCost(_pimpl->previous);
            }
            else
            {
                cost = edge->cost + _pimpl->costEstimates[*t];
            }
            smallestCost = min(smallestCost, cost);
        }
        // TODO
    }
*/

    return true;
}

SearchState LearningRealTimeAStarSearch::getState()
{
    return SearchState::UnInitialised;
}

std::vector<Edge*> LearningRealTimeAStarSearch::getResult()
{
    return std::vector<Edge*>();
}