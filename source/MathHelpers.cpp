#include "MathHelpers.h"
#include <random>
#include <iostream>

//#include "MHOperator.h"
#include "TypeHelpers.h"

using namespace std;

namespace MathHelpers
{
	long getReverse(const long& number)
	{
		string asString{to_string(number)};
		reverse(asString.begin(), asString.end());
		return stol(asString);
	}

	bool isPalindrome(const long& number)
	{
		long reversed = getReverse(number);
		return number == reversed;
	}

	std::vector<int> getRandomOrder(int numOfElements) 
	{
		std::vector<int> array;

		for (int i = 0;i<numOfElements;i++) {
			array.push_back(i);
		}
		std::random_shuffle(array.begin(), array.end());
		return array;
	}

	int getRandom(int min, int max)
	{
		static std::random_device rd;
		static std::mt19937 gen(rd());

		std::uniform_int_distribution<> dis(min, max);
		return dis(gen);
	}
    
    bool calculateLineCoefficients(const vec2& p1, const vec2& p2, float& outA, float& outB)
    {
        if (p1 == p2 // two same points do not form a line!
            || p2[0] == p1[0]) // line is of form "x = b", cannot be expressed thus this way
            return false;
        
        outA = (p2[1] - p1[1]) / (p2[0] - p1[0]);
        outB = p1[1] - outA * p1[0];
        
        return true;
    }

	bool isNearZero(float value, float limit)
	{
		return std::abs(value) <= std::abs(limit);
	}

	bool isSqrtInteger(long long value, long long& outSqrt) 
	{
		if (value == 0) 
		{
			return true;
		}
		double h = sqrt(value);
		double intpart;
		modf(h, &intpart);
		outSqrt = (long long) intpart;

		return (outSqrt * outSqrt) == value;
	}

	bool isInteger(double value) 
	{
		if (value == 0) 
		{
			return true;
		}
		double intpart;
		bool isInteger = (modf(value, &intpart) == 0.0);

		return isInteger;
	}

	/**
	* https://en.wikipedia.org/wiki/Greatest_common_divisor
	*/
	bool findGreatestCommonDivisor(int pa, int pb, int *gcd) {
		if (pa < 1 || pb < 1) {
			return false;
		}

		int a = pa;
		int b = pb;
		int d = 0;
		int g = 0;

		while ((a % 2 == 0) && (b % 2 == 0)) { // a and b are both even do
			a = a / 2;
			b = b / 2;
			d = d + 1;
		}
		while (a != b) {
			if (a % 2 == 0) {
				a = a / 2;
			}
			else if (b % 2 == 0) {
				b = b / 2;
			}
			else if (a > b) {
				a = (a - b) / 2;
			}
			else {
				b = (b - a) / 2;
			}
		}

		g = a;

		*gcd = (int)(g*pow(2, d));

		return true;
	}

	///////////////////////////////////////////////////////////////////////////////////
	// Shunting Yard - stuff
	///////////////////////////////////////////////////////////////////////////////////


	// operators
	// precedence   operators       associativity
	// 4            ^               right to left
	// 3            * / %           left to right
	// 2            + -             left to right
	// 1            =               right to left
	int op_preced(const char c)
	{
		switch (c) {
		case '^':
			return 4;
		case '*':  case '/': case '%':
			return 3;
		case '+': case '-':
			return 2;
		case '=': case '<': case '>':
			return 1;
		}
		return 0;
	}

	bool op_left_assoc(const char c)
	{
		switch (c) {
			// left to right
		case '*': case '/': case '%': case '+': case '-':
			return true;
			// right to left
		case '=': case '^': case '>': case '<':
			return false;
		}
		return false;
	}

	unsigned int op_arg_count(const char c)
	{
		switch (c) {
		case '*': case '/': case '%': case '+': case '-': case '=': case '<': case '>':
			return 2;
		case '^':
			//            return 1;
			return 2;
		default:
			return c - 'A';
		}
		return 0;
	}

#define is_operator(c)  (c == '+' || c == '-' || c == '/' || c == '*' || c == '^' || c == '%' || c == '=' || c == '>' || c == '<')
#define is_function(c)  (c >= 'A' && c <= 'Z')
#define is_ident(c)     ((c >= '0' && c <= '9') )
	//|| (c >= 'a' && c <= 'z')

	bool shunting_yard(const char *input, char *output)
	{
		const char *strpos = input, *strend = input + strlen(input);
		char c, *outpos = output;

		char stack[32];       // operator stack
		unsigned int sl = 0;  // stack length
		char     sc;          // used for record stack element

		while (strpos < strend) {
			// read one token from the input stream
			c = *strpos;
			if (c != ' ') {
				// If the token is a number (identifier), then add it to the output queue.
				if (is_ident(c)) {
					*outpos = c; ++outpos;
				}
				// If the token is a function token, then push it onto the stack.
				else if (is_function(c)) {
					stack[sl] = c;
					++sl;
				}
				// If the token is a function argument separator (e.g., a comma):
				else if (c == ',') {
					bool pe = false;
					while (sl > 0) {
						sc = stack[sl - 1];
						if (sc == '(') {
							pe = true;
							break;
						}
						else {
							// Until the token at the top of the stack is a left parenthesis,
							// pop operators off the stack onto the output queue.
							*outpos = sc;
							++outpos;
							sl--;
						}
					}
					// If no left parentheses are encountered, either the separator was misplaced
					// or parentheses were mismatched.
					if (!pe) {
						cout << "Error: separator or parentheses mismatched" << endl;
						return false;
					}
				}
				// If the token is an operator, op1, then:
				else if (is_operator(c)) {
					while (sl > 0) {
						sc = stack[sl - 1];
						// While there is an operator token, op2, at the top of the stack
						// op1 is left-associative and its precedence is less than or equal to that of op2,
						// or op1 has precedence less than that of op2,
						// Let + and ^ be right associative.
						// Correct transformation from 1^2+3 is 12^3+
						// The differing operator priority decides pop / push
						// If 2 operators have equal priority then associativity decides.
						if (is_operator(sc) &&
							((op_left_assoc(c) && (op_preced(c) <= op_preced(sc))) ||
								(op_preced(c) < op_preced(sc)))) {
							// Pop op2 off the stack, onto the output queue;
							*outpos = sc;
							++outpos;
							sl--;
						}
						else {
							break;
						}
					}
					// push op1 onto the stack.
					stack[sl] = c;
					++sl;
				}
				// If the token is a left parenthesis, then push it onto the stack.
				else if (c == '(') {
					stack[sl] = c;
					++sl;
				}
				// If the token is a right parenthesis:
				else if (c == ')') {
					bool pe = false;
					// Until the token at the top of the stack is a left parenthesis,
					// pop operators off the stack onto the output queue
					while (sl > 0) {
						sc = stack[sl - 1];
						if (sc == '(') {
							pe = true;
							break;
						}
						else {
							*outpos = sc;
							++outpos;
							sl--;
						}
					}
					// If the stack runs out without finding a left parenthesis, then there are mismatched parentheses.
					if (!pe) {
						cout << "Error: parentheses mismatched" << endl;
						return false;
					}
					// Pop the left parenthesis from the stack, but not onto the output queue.
					sl--;
					// If the token at the top of the stack is a function token, pop it onto the output queue.
					if (sl > 0) {
						sc = stack[sl - 1];
						if (is_function(sc)) {
							*outpos = sc;
							++outpos;
							sl--;
						}
					}
				}
				else {
					 cout << "Unknown token " << c << endl;
					return false; // Unknown token
				}
			}
			++strpos;
		}
		// When there are no more tokens to read:
		// While there are still operator tokens in the stack:
		while (sl > 0) {
			sc = stack[sl - 1];
			if (sc == '(' || sc == ')') {
				cout << "Error: parentheses mismatched" << endl;
				return false;
			}
			*outpos = sc;
			++outpos;
			--sl;
		}
		*outpos = 0; // Null terminator
		return true;
	}

	bool execution_order(const char* input, std::vector<double> *dataArray) {
		const char *strpos = input, *strend = input + strlen(input);
		char c, res[4];
		unsigned int sl = 0, sc, stack[32], rn = 0;
		// While there are input tokens left
		while (strpos < strend) {
			// Read the next token from input.
			c = *strpos;
			// If the token is a value or identifier
			if (is_ident(c)) {
				// Push it onto the stack.
				stack[sl] = c;
				++sl;
			}
			// Otherwise, the token is an operator  (operator here includes both operators, and functions).
			else if (is_operator(c) || is_function(c)) {
				sprintf(res, "_%02d", rn);
				//printf("%s = ", res);
				++rn;
				// It is known a priori that the operator takes n arguments.
				unsigned int nargs = op_arg_count(c);
				// If there are fewer than n values on the stack
				if (sl < nargs) {
					// (Error) The user has not input sufficient values in the expression.
					return false;
				}
				// Else, Pop the top n values from the stack.
				// Evaluate the operator, with the values as arguments.
				if (is_function(c)) {
					//printf("%c(", c);
					while (nargs > 0) {
						sc = stack[sl - nargs]; // to remove reverse order of arguments
						//if (nargs > 1) {
						//	printf("%s, ", &sc);
						//}
						//else {
						//	printf("%s)\n", &sc);
						//}
						--nargs;
					}
					sl -= op_arg_count(c);
				}
				else {
					if (nargs == 1) {
						sc = stack[sl - 1];
						sl--;
						//printf("%c %s;\n", c, &sc);
					}
					else {
						std::string str1 = "";
						std::string str2 = "";
						str1 += (char)stack[sl - 2];
						str2 += (char)stack[sl - 1];

						double value1 = dataArray->at(atoi(str1.c_str()));
						double value2 = dataArray->at(atoi(str2.c_str()));

						double resValue = 0;

						if (c == '+') {
							resValue = value1 + value2;
						}
						else if (c == '-') {
							resValue = value1 - value2;
						}
						else if (c == '*') {
							resValue = value1 * value2;
						}
						else if (c == '/') {
							if (value2 == 0) {
								resValue = 0;
								return false; // Divide by zero is not allowed
							}
							else {
								resValue = value1 / (double)value2;
							}
						}
						else if (c == '^') {
							resValue = pow(value1, value2);
						}
						else if (c == '=') {
							resValue = (fabs(value1 - value2) < 0.00001) ? 1 : 0;
						}
						else if (c == '<') {
							resValue = (value1 < value2) ? 1 : 0;
						}
						else if (c == '>') {
							resValue = (value1 > value2) ? 1 : 0;
						}

						//                    dataArray->insert(dataArray->begin(), resValue);
						dataArray->push_back(resValue);

						sc = stack[sl - 2];
						//printf("%s %c ", &sc, c);
						sc = stack[sl - 1];
						sl--;sl--;
						//printf("%s;\n", &sc);
					}
				}
				// Push the returned results, if any, back onto the stack.
				std::string stackStr = to_string(dataArray->size() - 1);
				//printf("stackStr = %s\n", stackStr.c_str());
				stack[sl] = stackStr.c_str()[0]; // This allows only 10 values!! Fix it?!
				++sl;
			}
			++strpos;
		}
		// If there is only one value in the stack
		// That value is the result of the calculation.
		if (sl == 1) {
			sc = stack[sl - 1];
			sl--;
			std::string tmp = "";
			tmp += sc;

			double value = dataArray->at(atoi(tmp.c_str()));
			//printf("%f is a result\n", value);

			return (value > 0);
		}
		// If there are more values in the stack
		// (Error) The user input has too many values.
		return false;
	}

	bool isTokenOk(const std::string &token) {
		std::size_t dotFound = token.find(".");
		if (dotFound != std::string::npos) {
			return true;
		}
		std::size_t commaFound = token.find(",");
		if (commaFound != std::string::npos) {
			return true;
		}

		std::string prefix1 = "0";
		std::string prefix2 = "-0";

		if (token.length() > 1 && ((!token.compare(0, prefix1.size(), prefix1)) ||
			(!token.compare(0, prefix2.size(), prefix2)))) {
			return false;
		}

		return true;
	}

/*
	bool isEquationValid(const Vector<MHTerm*> &sortedTerms) {
		std::string realStr = "";
		std::string token = "";
		std::string accu = "";
		std::vector<double> identifiers;
		int idCounter = 0;

		// TODO: Take leading sign in to account
		MHTerm* prev = 0;
		bool sign = false;
		bool invalidNumber = false;

		for (int i = 0;i<sortedTerms.size() - 1;i++) {
			sign = false;

			auto nodeLeft = sortedTerms.at(i);
			auto nodeRight = sortedTerms.at(i + 1);

			// Negative number can have minus first or after operator
			if ((i == 0 || (prev != 0 && MHTerm::isOperator(*prev))) && nodeLeft->getType() == MHTerm::Type::MINUS) {
				sign = true;
			}

			if (i == 0 && !sign && MHTerm::isOperator(*nodeLeft)) {
				// Can't have an operator as the first term
				std::cout << "Can't have an operator as the first term" << std::endl;
				invalidNumber = true;
				break;
			}

			if (nodeLeft->getType() == MHTerm::Type::MINUS && nodeRight->getType() == MHTerm::Type::MINUS) {
				std::cout << "Can't have two successive minus signs without parenthesis" << std::endl;
				invalidNumber = true;
				break;
			}

			if (nodeLeft->getType() == MHTerm::Type::FRACTION && nodeRight->getType() == MHTerm::Type::FRACTION) {
				std::cout << "Can't have two successive fraction terms" << std::endl;
				invalidNumber = true;
				break;
			}

			if ((MHTerm::isNumberOrFraction(*nodeLeft) || sign)) { // NUMBER

																   // Number or sign next to function, symbol or number
				if (MHTerm::isNumberOrFraction(*nodeRight) || nodeRight->getType() == MHTerm::Type::POWER) {
					if (sign) {
						// minus sign
						identifiers.push_back(-1.0);
						accu = accu + to_string(idCounter) + "*";
						idCounter++;
						token = "";
					}
					else {
						auto number = dynamic_cast<MHNumber*>(nodeLeft);
						double value = number->getBaseValue();
						if (prev && MHTerm::isNumberOrFraction(*prev) && value < 0) { // avoid tokens like "3-5"
							if (token.length() > 0) {
								if (!isTokenOk(token)) {
									invalidNumber = true;
									std::cout << "Invalid number" << std::endl;
									break;
								}
								identifiers.push_back(atof(token.c_str()));
								accu = accu + to_string(idCounter);
								idCounter++;
								token = "";
							}
						}

						token = token + to_string(number->getBaseValue());
					}
				}
				if (nodeRight->getType() == MHTerm::Type::SYMBOL || MHTerm::isOperator(*nodeRight)) { // BUG with -|3-2|=1-2 should be fixed now
					auto number = dynamic_cast<MHNumber*>(nodeLeft);
					if (nodeLeft->getType() == MHTerm::Type::MINUS) {
						if (nodeRight->getType() == MHTerm::Type::SYMBOL) {
							token = token + "-1";
						}
						else {
							invalidNumber = true; // Can't have minus and operator next to each other
							break;
						}
					}
					else {
						double value = number->getBaseValue();
						if (prev && MHTerm::isNumberOrFraction(*prev) && value < 0) { // avoid tokens like "3-5"
							if (token.length() > 0) {
								if (!isTokenOk(token)) {
									invalidNumber = true;
									std::cout << "Invalid number" << std::endl;
									break;
								}
								identifiers.push_back(atof(token.c_str()));
								accu = accu + to_string(idCounter);
								idCounter++;
								token = "";
							}
						}

						token = token + to_string(number->getBaseValue());
					}

					if (token.length() > 0) {
						if (!isTokenOk(token)) {
							invalidNumber = true;
							std::cout << "Invalid number" << std::endl;
							break;
						}
						identifiers.push_back(atof(token.c_str()));
						accu = accu + to_string(idCounter);
						idCounter++;
						token = "";
					}
				}

			}
			else if (nodeLeft->getType() == MHTerm::Type::POWER) { // POWER
				auto number = dynamic_cast<MHNumber*>(nodeLeft);
				auto numberR = dynamic_cast<MHNumber*>(nodeRight);

				if (MHTerm::isNumberOrFraction(*nodeRight) && numberR->getCalculatedValue() >= 0) { // Can't have power and function/symbol/number next to each other
					std::cout << "Can't have power and number next to each other" << std::endl;
					invalidNumber = true;
					break;
				}
				token = token + to_string(number->getBaseValue());

				if (token.length() > 0) {
					if (!isTokenOk(token)) {
						invalidNumber = true;
						std::cout << "Invalid number" << std::endl;
						break;
					}

					// We don't have parentheses, so all powers are from positive numbers
					double val = atof(token.c_str());
					if (val < 0) {
						if (idCounter == 0) { // If power is first term and it has a negative sign
							identifiers.push_back(-1.0);
							accu = accu + to_string(idCounter) + "*";
							idCounter++;
						}
						else {
							accu = accu + "-";
						}

						val = -val;
					}
					identifiers.push_back(val);
					accu = accu + to_string(idCounter);
					idCounter++;
					token = "";
				}

				identifiers.push_back(number->getExponentValue());
				accu = accu + "^" + to_string(idCounter);
				idCounter++;

			}
			else if (nodeLeft->getType() == MHTerm::Type::SYMBOL) { // SYMBOL
				auto number = dynamic_cast<MHNumber*>(nodeLeft);
				auto numberR = dynamic_cast<MHNumber*>(nodeRight);

				if (MHTerm::isNumberOrFraction(*nodeRight) && numberR->getCalculatedValue() >= 0) { // Can't have symbol and number next to each other
					std::cout << "Can't have symbol and number next to each other" << std::endl;
					invalidNumber = true;
					break;
				}
				if (prev != 0 && ((!MHTerm::isOperator(*prev)) || (prev->getType() == MHTerm::Type::MINUS))) { // If symbol is preceded by something else other than operator then we need to add multiplication operator
					accu = accu + "*";
				}

				identifiers.push_back(number->getBaseValue());
				accu = accu + to_string(idCounter);
				idCounter++;
			}
			else if (MHTerm::isOperator(*nodeLeft)) { // OPERATOR
				if (MHTerm::isOperator(*nodeRight) && nodeRight->getType() != MHTerm::Type::MINUS) {
					// Two operators next to each other which is not allowed (expect when latter is minus sign)
					std::cout << "Two operators next to each other" << std::endl;
					invalidNumber = true;
					break;
				}
				accu += MHOperator::getCharForType(nodeLeft->getType()); // charMap[nodeLeft->getType()]; asdfsafd
			}

			// Add last term
			if (((i + 1) == (sortedTerms.size() - 1))) { // Last number ready to be added, preceded by number or sign
				if (MHTerm::isOperator(*nodeRight)) {
					// We can't have an operator as the last term
					std::cout << "Last term can't be an operator" << std::endl;
					invalidNumber = true;
					break;
				}

				auto number = dynamic_cast<MHNumber*>(nodeRight);
				if (MHTerm::isNumberOrFraction(*nodeRight)) {
					double value = number->getBaseValue();
					if (value < 0 && !MHTerm::isOperator(*nodeLeft)) {
						if (token.length() > 0) { // Add previous term
							if (!isTokenOk(token)) {
								invalidNumber = true;
								std::cout << "Invalid number" << std::endl;
								break;
							}
							identifiers.push_back(atof(token.c_str()));
							accu = accu + to_string(idCounter);
							idCounter++;
							token = "";
						}

						accu = accu + "-";
						identifiers.push_back(-value);
						accu = accu + to_string(idCounter);
						idCounter++;
						token = "";
						continue;
					}
					else {
						token = token + to_string(number->getBaseValue());
					}

					if (token.length() > 0) {
						if (!isTokenOk(token)) {
							std::cout << "Invalid number" << std::endl;
							invalidNumber = true;
							break;
						}

						identifiers.push_back(atof(token.c_str()));
						accu = accu + to_string(idCounter);
						token = "";
						idCounter++;
					}
				}
				else if (nodeRight->getType() == MHTerm::Type::POWER) {
					token = token + to_string(number->getBaseValue());

					if (token.length() > 0) {
						if (!isTokenOk(token)) {
							invalidNumber = true;
							std::cout << "Invalid number" << std::endl;
							break;
						}

						// We don't have parentheses, so all powers are from positive numbers
						double val = atof(token.c_str());
						if (val < 0) {
							accu = accu + "-";
							val = -val;
						}
						identifiers.push_back(val);
						accu = accu + to_string(idCounter);
						idCounter++;
						token = "";
					}

					identifiers.push_back(number->getExponentValue());
					accu = accu + "^" + to_string(idCounter);
					idCounter++;
				}
				else if (nodeRight->getType() == MHTerm::Type::SYMBOL) {
					if (nodeLeft != 0 && !MHTerm::isOperator(*nodeLeft)) { // If symbol is preceded by something else other than operator then we need to add multiplication operator
						accu = accu + "*";
					}

					identifiers.push_back(number->getBaseValue());
					accu = accu + to_string(idCounter);
					idCounter++;
				}

				break;
			}

			prev = nodeLeft;
		}

		//for (double d : identifiers) {
		//    printf("identifier: %f\n", d);
		//}
		char output[128];
		bool answer = false;

		if (!invalidNumber) {
			//printf("accu: %s\n", accu.c_str());
			MathHelpers::shunting_yard(accu.c_str(), output);
			//printf("output: %s\n", output);
			answer = MathHelpers::execution_order(output, &identifiers);
		}

		return answer;
	}*/
}