#define MEASURE_TIME

#include "OctreeHelpers.h"
#include <vector>
#include "Octree.h"

#include "Profiler.h"
#include "SimpleTimer.h"
#include "Debug.h"

using namespace std;
using namespace time_helpers;

static std::string OctreeProfilerName{"octree_profiler"};

namespace OctreeHelpers
{
	Octree* makeTriangleOctree(vector<Triangle>& triangles, vec3 origin, vec3 halfDimension)
	{
		Octree* octree = new Octree(nullptr, origin, halfDimension);

		int count = triangles.size();
		// TODO: this is dirty, no-one owns currently the data, so it is not deallocated
		// FIX THIS
		OctreeDataTriangle** data = new OctreeDataTriangle*[count];
		for (int i = 0; i < triangles.size(); ++i)
		{
			data[i] = new OctreeDataTriangle();
			data[i]->setData(&(triangles[i]));
			octree->insert(data[i]);
		}
		return octree;
	}

	Octree* makeVertexOctree(vector<vec3>& vertices, vec3 origin, vec3 halfDimension)
	{
		Octree* octree = new Octree(nullptr, origin, halfDimension);

		int count = vertices.size();
		// TODO: this is dirty, no-one owns currently the data, so it is not deallocated
		// FIX THIS
		OctreeDataVertex** data = new OctreeDataVertex*[count];
		for (int i = 0; i < vertices.size(); ++i)
		{
			data[i] = new OctreeDataVertex();
			data[i]->setData(&(vertices[i]));
			octree->insert(data[i]);
		}
		return octree;
	}


	float clipToRange(float value, float cp, float hd, float offset = 0.0f)
	{
		auto temp = max(value, cp - hd + offset);
		return min(temp, cp + hd - offset);
	}

	vec3 getProjectionToBoundingBox(Octree* root, const vec3& point, float offset = 0.0f)
	{
		auto origo = root->getOrigin();
		auto halfDimension = root->getHalfDimension();

		// because our bounding box is axis aligned, we can just simply "flatten" the point
		return vec3(clipToRange(point[0], origo[0], halfDimension[0], offset)
			, clipToRange(point[1], origo[1], halfDimension[1], offset)
			, clipToRange(point[2], origo[2], halfDimension[2], offset));
	}

	// TODO: this is meant for Triangle-mode, and does not work properly atm
	void getClosestDataHelper(Octree* root
		, Octree** resultNode
		, OctreeData** resultData
		, const vec3& point
		, vec3& closestSoFar
		, float& closestDistanceSoFar)
	{
		// if there is leaf data, check distance to that
		if (root->getData() != nullptr)
		{
			auto closest = root->getData()->getClosest(point);
			auto distanceSquared = closest.squaredNorm();

			if (distanceSquared < closestDistanceSoFar)
			{
				closestDistanceSoFar = distanceSquared;
				closestSoFar = closest;
				*resultNode = root;
				*resultData = root->getData();
			}
		}

		// ... then check distances to all the datas that fit into this node
		for (int i = 0; i < root->getFullyContainedDataCount(); i++)
		{
			auto data = root->getFullyContainedData(i);
			auto closest = data->getClosest(point);
			auto diff = closest - point;
			auto squared = diff.squaredNorm();
			if (squared < closestDistanceSoFar)
			{
				closestDistanceSoFar = squared;
				closestSoFar = closest;
				*resultNode = root;
				*resultData = data;
			}
		}

		// lowest level so can stop?
		if (root->isLeafNode())
		{
			return;
		}

		// need to recurse
		auto child = root->getFirstChildForPoint(&point);
		getClosestDataHelper(child, resultNode, resultData, point, closestSoFar, closestDistanceSoFar);

		// TODO: tämä ei ota ollenkaan huomioon rekursiota ylöspäin/parenttiin,
		// eli jos ollaan ylipäätään valittu node jossa ei ole mitään dataa,
		// niin tämä ei tutki ylöspäin -> vai pitäiskö ton fullyEnclosedin pitää huoli tästä????
		// - eli, jos tulos on null, pitää mennä getClosestLeafWithData() tavoin ylöspäin!!!
		//PASKA

	}

	// TODO: this is meant for Triangle-mode, and does not work properly atm
	void getClosestData(Octree* octree
		, const vec3& point
		, Octree** outRootCell
		, Octree** outCell
		, OctreeData** outData
		, bool projectIfOutside)
	{
		vec3 closestPoint;
		auto closestDistanceSquared = std::numeric_limits<float>::max();

		vec3 reference = point;

		if (!octree->containsPoint(&point))
		{
			reference = getProjectionToBoundingBox(octree, point, 0.001f);
		}

		// base case, take the first node that has fully enclosed data in the same cell as the given point

		*outRootCell = octree->getFirstEnclosingNodeForPoint(&reference);

		if (*outRootCell == nullptr)
		{
			// pathological case, should not happen if projectIfOutside == true
			return;
		}

		// ... and the closest data
		getClosestDataHelper(*outRootCell, outCell, outData, reference, closestPoint, closestDistanceSquared);
	}

	// NOTE! This routine is point/position based
	// It does not take into account the geometry of the data, treat it as a point
	Octree* getClosestLeafWithData(Octree* root, const vec3& point, bool projectIfOutside)
	{
		PROFILE_BLOCK(octreeProfilerName, "getClosestLeafWithData");

		vec3 reference = point;

		// take the actual oct tree cell containing the wanted point
		auto cell = root->getLeafForPoint(&reference);

		if (cell == nullptr && projectIfOutside)
		{
			PROFILE_BLOCK(OctreeProfilerName, "point outside, projecting");

			// no result, the point is totally outside of our oct tree bounding box
			// -> project the point to our bounding box and continue from there
			reference = getProjectionToBoundingBox(root, point, 0.001f);
			cell = root->getLeafForPoint(&reference);
		}
		
		if (cell == nullptr)
		{
			// pathlogical case, should not ever happen (if "projectIfOutside" == true)
			return nullptr;
		}

		// our result contains data, we have our result right here
		if (cell->getData() != nullptr)
		{
			return cell;
		}

		// otherwise, take parents first child that has data
		auto parent = cell->getParent();
		if (parent == nullptr)
		{
			return nullptr;
		}
		auto first = parent->getFirstWithData();
		if (first->getData() == nullptr)
		{
			return nullptr;
		}

		// as a naive result, this could be our final result, but it is
		// possible that adjacent oct tree nodes in same hiearchy level are closer,
		// so we use this to form a search cube

		auto halfLength = (*first->getData()->getPosition() - point).norm();
		
		auto bbMin = point - halfLength * vec3::Ones();
		auto bbMax = point + halfLength * vec3::Ones();

		vector<Octree*> results;
		root->getPointsInsideBox(bbMin, bbMax, results);

		if (results.size() == 0)
			return first;

		// pick the closest one with brute force, order so that smallest distance first
		sort(results.begin(), results.end(), [&](Octree* p1, Octree* p2)
		{
			auto p1DistSquared = (point - *p1->getData()->getPosition()).squaredNorm();
			auto p2DistSquared = (point - *p2->getData()->getPosition()).squaredNorm();
			return p1DistSquared < p2DistSquared;
		});


		return results[0];
	}
}