
#include "GraphHeurestics.h"
#include "GraphUtilities.h"

namespace graph_heurestics
{
    using namespace graph_heurestics;
    using namespace graph_utilities;

    float euclideanDistanceSquared(const Graph* graph, const Node* node, const Node* goal)
    {
        return (goal->position - node->position).squaredNorm();
    }
}