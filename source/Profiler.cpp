//#include "Platform.h"
#include <cstring>
#include <iostream>

#include <cstdio>
#include <ctime>
#include <iostream>
#include <fstream>
#include "Memory.h"

#include "Profiler.h"
#include "Debug.h"
#include "Exception.h"

using namespace time_helpers;

Profiler::ProfiledBlock::ProfiledBlock(Profiler* profiler, const char * name)
{
	m_profiler = profiler;
	/// @todo TODO: store the profiler sample pointer here!!
	m_name = name;
	profiler->begin(m_name);
}

Profiler::ProfiledBlock::~ProfiledBlock()
{
	m_profiler->end(m_name);
}

unsigned int Profiler::sm_nestedSampleIndentation = 1;

Profiler::Profiler(const char * name) 
: m_frameStartTime(0.0)
, m_frameEndTime(0.0)
, m_lastTimeTick(0.001)
, m_percentages(false)
, m_fpsSampleCount(0)
, m_fpsOldestSampleIndex(0)
, m_fps(0.0)
, m_name(name)
, m_systemStartTime(0.0)
{
	
	for (int s = 0; s < NUM_SAMPLES; s++) 
	{
		m_samples[s].valid = false;
	}	
	
	for (int h = 0; h < NUM_SAMPLES_HISTORY; h++) 
	{
		m_history[h].valid = false;
	}
	
	//m_systemStartTime = getExactTimeSeconds();

	initialize();
}

Profiler::~Profiler()
{}

void Profiler::setNestedSampleIndentation(unsigned int indentation)
{
	sm_nestedSampleIndentation = indentation;
}

void Profiler::initialize( void )
{
	m_frameStartTime = getExactTime();   
	m_profilingStartTime = m_frameStartTime;
	m_lastFrameDuration = 0;

	m_timer.restart();

	markFrameEnd(m_frameStartTime);
}

void Profiler::unInitialize()
{

}

double Profiler::getExactTime()
{
	// returns profile time in seconds
	auto now = m_timer.getElapsed();
	return (now - m_systemStartTime);
}

double Profiler::getLastFrameDuration() const
{
	return m_lastFrameDuration;
}

void Profiler::markFrameEnd(double currentTimeSeconds)
{	
	m_frameEndTime = currentTimeSeconds;
	m_lastFrameDuration = m_frameEndTime - m_frameStartTime;
	if( m_lastFrameDuration < 0 ) 
	{
		m_lastFrameDuration = 0;
	}
}

void Profiler::begin( const char * name )
{
	int i = 0;

	// find first free sample point, or pre-existing "started" sample with the same name
	while (i < NUM_SAMPLES && m_samples[i].valid)
	{
		if (strcmp(m_samples[i].name, name) == 0) 
		{
			//Found the sample
			m_samples[i].openProfilers++;
			m_samples[i].profilerInstances++;
			m_samples[i].startTime = getExactTime();
			
			CUSTOM_ASSERT1( m_samples[i].openProfilers == 1 ); //max 1 open at once
			return;
		}
		i++;	
	}

	if( i >= NUM_SAMPLES ) 
	{
		INFO_PRINT("Exceeded Max Available Profiler Samples: %d (max: %d)!", i, NUM_SAMPLES);
		CUSTOM_ASSERT1(false);
		return;
	}

	//strcpy_s(m_samples[i].name, ProfilerSample::NAME_MAX_LENGTH, name);
	strcpy(m_samples[i].name, name);
	m_samples[i].valid = true;
	m_samples[i].openProfilers = 1;
	m_samples[i].profilerInstances = 1;
	m_samples[i].accumulatorTime = 0.0;
	m_samples[i].startTime = getExactTime();
	m_samples[i].childrenSampleTime = 0;
}

void Profiler::end( const char * name )
{
	int i = 0;
	unsigned int numParents = 0;

	while (i < NUM_SAMPLES && m_samples[i].valid)
	{
		if (strcmp(m_samples[i].name, name) == 0)
		{  
			CUSTOM_ASSERT1(m_samples[i].valid); // make sure that begin() has been called
			//Found the sample
			unsigned int inner = 0;
			int parent = -1;
			double endTimeSeconds = getExactTime();
			m_samples[i].openProfilers--;

			//Count all parents and find the immediate parent
			while( m_samples[inner].valid == true ) {
				if( m_samples[inner].openProfilers > 0 )
				{  //Found a parent (any open Profilers are parents)
					numParents++;
					if( parent < 0 )
					{  //Replace invalid parent (index)
						parent = inner;
					}
					else if( m_samples[inner].startTime >=
						m_samples[parent].startTime )
					{  //Replace with more immediate parent
						parent = inner;
					}
				}
				inner++;
			}

			//Remember the current number of parents of the sample
			m_samples[i].numberOfParents = numParents;

			if( parent >= 0 )
			{  //Record this time in fChildrenSampleTime (add it in)
				m_samples[parent].childrenSampleTime += endTimeSeconds -
					m_samples[i].startTime;
			}

			//Save sample time in accumulator
			m_samples[i].accumulatorTime += 
				endTimeSeconds - m_samples[i].startTime;
			return;
		}
		i++;	
	}
}

void Profiler::update()
{
	int i = 0;

	markFrameEnd(getExactTime());

	while (i < NUM_SAMPLES && m_samples[i].valid) 
	{		
		double sampleTime;
		double percentTime; //, aveTime, minTime, maxTime;

		if( m_samples[i].openProfilers < 0 || m_samples[i].openProfilers > 0) 
		{
			INFO_PRINT("Profiler::update() called when profiler block \"%s\" was open (openProfilers=%d, numberOfParents=%d, valid=%s)!", 
				m_samples[i].name, m_samples[i].openProfilers,
				m_samples[i].numberOfParents, (m_samples[i].valid) ? "true" : "false");

			CUSTOM_ASSERT1(false); // !"Profiler::update() called when profiler had samples open" );	
		}

		// @todo TODO: re-enable subtracting child measurements with a flag, or store 2nd set of values with child times subtracted
		// - m_samples[i].childrenSampleTime;
		sampleTime = m_samples[i].accumulatorTime; 
		percentTime = ( 1.0 * sampleTime / (m_frameEndTime - m_frameStartTime ) ) * 100.0;

		// update analysis information
		AnalysisInformation & ai = m_analysisInformation[m_samples[i].name];
		ai.currentTimeSeconds = sampleTime;
		ai.currentTimePercent = percentTime;

		//Add new measurement into the history and get the ave, min, and max
		storeInHistory(m_samples[i].name, m_samples[i].numberOfParents, sampleTime, percentTime);
		getFromHistory(m_samples[i].name, &ai.averageTimeSeconds, &ai.fastestTimeSeconds, &ai.slowestTimeSeconds);

		CUSTOM_ASSERT1(m_analysisInformation[m_samples[i].name].averageTimeSeconds == ai.averageTimeSeconds);
		i++;
	}

	// Reset samples for next frame
	for (int j = 0; j < NUM_SAMPLES; j++) 
	{
		m_samples[j].valid = false;
	}

	// Start new frame
	m_frameStartTime = getExactTime();
	updateFrameRateCounter(m_frameStartTime);
}

size_t Profiler::writeFormattedOutput( char * outputBuffer, size_t outputBufferLen, size_t* numberOfLines_out ) const
{
	size_t outputBufferLenLeft = outputBufferLen;
	snprintf(outputBuffer, outputBufferLenLeft, "%s FPS: %.2f\n", m_name, m_fps);
	if (numberOfLines_out!=NULL) (*numberOfLines_out) = 1;
	size_t len = strlen(outputBuffer);
	outputBuffer += len;
	if (outputBufferLenLeft>len) {
		outputBufferLenLeft-=len;
	}
	else
	{
		CUSTOM_ASSERT1(false); // out of buffer size
		return outputBufferLen;
	}
	
	int i = 0;
	while (i < NUM_SAMPLES) 
	{		
		unsigned int indent = 0;

		AnalysisInformationMap::const_iterator it = m_analysisInformation.find(m_samples[i].name);
		if (it==m_analysisInformation.end()) { 
			i++;
			continue;
		}
		const AnalysisInformation & ai = (*it).second;

		// make an indention of sm_nestedSampleIndentation for each nested profiling
		char name[256], indentedName[256];
		//strcpy_s(indentedName, 256, m_samples[i].name );
		strcpy(indentedName, m_samples[i].name );

		for (indent = 0; indent < m_samples[i].numberOfParents; indent++ ) 
		{
			for (unsigned int nsi = 0; nsi < sm_nestedSampleIndentation; nsi++)
			{
				snprintf(name, 256, " %s", indentedName );
				//strcpy_s(indentedName, 256, name );
				strcpy(indentedName, name );
			}
		}

		sprintf(outputBuffer, "%s %.2fms (%.1f%%)\n"
			, indentedName
			, ai.currentTimeSeconds
			, ai.currentTimePercent);

		size_t outlen = strlen(outputBuffer);
		
		outputBuffer += outlen;
		
		if (outputBufferLenLeft>outlen) 
		{
			outputBufferLenLeft-=outlen;
		} 
		else 
		{
			CUSTOM_ASSERT1(false); // out of output buffer size
			return outputBufferLen; // 
		}
		if (numberOfLines_out!=NULL) (*numberOfLines_out)++;
		
		i++;
	}
	return outputBufferLen-outputBufferLenLeft;
}

size_t Profiler::writeFormattedHistoryOutput(char * outputBuffer
	, size_t outputBufferLen
	, size_t* numberOfLines_out  ) const
{
	size_t outputBufferLenLeft = outputBufferLen;

	// update history text buffer
	double aveTime;
	
	int i = 0;
	
	snprintf(outputBuffer
		, outputBufferLenLeft
		, "%s FPS: %.2f (last frame %i ms)\n"
		, m_name
		, m_fps
		, (int)(getLastFrameDuration()));

	size_t len = strlen(outputBuffer);
	outputBuffer += len;
	if (numberOfLines_out!=NULL) (*numberOfLines_out) = 1;
	if (outputBufferLenLeft>len) 
	{
		outputBufferLenLeft-=len;
	} 
	else 
	{
		CUSTOM_ASSERT1(false); // out of buffer size
		return outputBufferLen;
	}

	while (i < NUM_SAMPLES_HISTORY ) 
	{
		if (!m_history[i].valid) { i++; continue; }

		// convert from seconds to ms
		aveTime = m_history[i].aveTime;			
		
		// make an indention of 1 for each nested profiling
		char name[256], indentedName[256];
		//strcpy_s(indentedName, 256, m_history[i].name );
		strcpy(indentedName, m_history[i].name );
		for (unsigned int indent = 0; indent < m_history[i].numberOfParents; indent++ ) 
		{
			for (unsigned int nsi = 0; nsi < sm_nestedSampleIndentation; nsi++)
			{
				snprintf(name, 256, " %s", indentedName );
				//strcpy_s(indentedName, 256, name );
				strcpy(indentedName, name );
			}
		}

		sprintf(outputBuffer
			, "%d: %s %.2fms (%.1f%%)\n"
			,  m_history[i].numberOfParents
			, indentedName
			, aveTime
			, m_history[i].avePct);

		len = strlen(outputBuffer);
		outputBuffer += len;
		
		if (outputBufferLenLeft>len) 
		{
			outputBufferLenLeft-=len;
		}
		else 
		{
			CUSTOM_ASSERT1(false); // out of output buffer size
			return outputBufferLen; // 
		}
		if (numberOfLines_out!=NULL) (*numberOfLines_out)++;
		
		i++;
	}
	return outputBufferLen-outputBufferLenLeft;
}

void Profiler::storeInHistory( const char * name, int numberOfParents, 
							  double sample, double percent )
{
	int i = 0;
	double oldRatio;
	double newRatio = 0.8 * getLastFrameDuration();
	if( newRatio > 1.0 ) 
	{
		newRatio = 1.0;
	}
	oldRatio = 1.0 - newRatio;

	while( i < NUM_SAMPLES_HISTORY && m_history[i].valid == true ) 
	{
		if( strcmp(m_history[i].name, name) == 0 )
		{  
			//Found the sample
			m_history[i].timesCalled++;
			
			//m_history[i].numberOfParents = numberOfParents;
				
			// percentages
			m_history[i].avePct = (m_history[i].avePct*oldRatio) + (percent*newRatio);
			if (percent < m_history[i].minPct) 
				m_history[i].minPct = percent;
			else 
				m_history[i].minPct = (m_history[i].minPct*oldRatio) + (percent*newRatio);
			if (m_history[i].minPct <= 0.0) 
				m_history[i].minPct = 0.0;
			if (m_history[i].maxPct <= 0.0) 
				m_history[i].maxPct = 0.0;
			if (m_history[i].avePct <= 0.0) 
				m_history[i].avePct = 0.0;
			if (percent > m_history[i].maxPct) 
				m_history[i].maxPct = percent;
			else 
				m_history[i].maxPct = (m_history[i].maxPct*oldRatio) + (percent*newRatio);
			
			// absolute time
			m_history[i].aveTime = (m_history[i].aveTime*oldRatio) + (sample*newRatio);
			if (sample < m_history[i].minTime) 
				m_history[i].minTime = sample;
			else 
				m_history[i].minTime = (m_history[i].minTime*oldRatio) + (sample*newRatio);
			if (m_history[i].minTime <= 0.0) 
				m_history[i].minTime = 0.0;
			if (m_history[i].maxTime <= 0.0) 
				m_history[i].maxTime = 0.0;
			if (m_history[i].aveTime <= 0.0) 
				m_history[i].aveTime = 0.0;
			if (sample > m_history[i].maxTime) 
				m_history[i].maxTime = sample;
			else 
				m_history[i].maxTime = (m_history[i].maxTime*oldRatio) + (sample*newRatio);
			return;
		}
		i++;
	}

	if( i < NUM_SAMPLES_HISTORY )
	{  //Add to history
		m_history[i].numberOfParents = numberOfParents;
		//strcpy_s(m_history[i].name, ProfilerSampleHistory::NAME_MAX_LENGTH, name);
		strcpy(m_history[i].name, name);
		m_history[i].timesCalled = 1;
		m_history[i].valid = true;
		m_history[i].avePct = m_history[i].minPct = m_history[i].maxPct = percent;
		m_history[i].aveTime = m_history[i].minTime = m_history[i].maxTime = sample;
	}
	else 
	{
		CUSTOM_ASSERT1(false); // !"Exceeded Max Available Profiler History Samples!");
	}
}

void Profiler::getFromHistory( const char * name, double* ave, double* min, double* max )
{
	int i = 0;
	while( i < NUM_SAMPLES_HISTORY && m_history[i].valid == true ) 
	{
		if( m_history[i].name == name )
		{  
			//Found the sample
			if (m_percentages)
			{
				*ave = m_history[i].avePct;
				*min = m_history[i].minPct;
				*max = m_history[i].maxPct;
			}
			else
			{
				// convert from seconds to ms
				*ave = m_history[i].aveTime;
				*min = m_history[i].minTime;
				*max = m_history[i].maxTime;
			}
			return;
		}
		i++;
	}	
	*ave = *min = *max = 0.0;
}

void Profiler::updateFrameRateCounter(double frameStartTimeSeconds)
{
	// update fps
	if (m_fpsSampleCount > 0) 
	{
		m_fps = static_cast<float>(m_fpsSampleCount / static_cast<double> (frameStartTimeSeconds - m_fpsFrameStartTimes[m_fpsOldestSampleIndex]));
	}
	else
	{
		m_fps = 0.0f;
	}
	// add new sample
	if (m_fpsSampleCount < FPS_NUM_SAMPLES)
	{
		m_fpsOldestSampleIndex = 0; 	
		// store frame start times
		m_fpsFrameStartTimes[m_fpsSampleCount] = frameStartTimeSeconds;
		m_fpsSampleCount++;
	}
	else
	{
		// replace oldest sample with new one
		m_fpsFrameStartTimes[m_fpsOldestSampleIndex] = frameStartTimeSeconds;
		// oldest sample is now the newest sample index + 1
		m_fpsOldestSampleIndex = (m_fpsOldestSampleIndex + 1) % FPS_NUM_SAMPLES;
	}
}

Profiler::AnalysisInformation::AnalysisInformation() 
	: averageTimeSeconds(0.0f)
	, fastestTimeSeconds(0.0f)
	, slowestTimeSeconds(0.0f)
	, currentTimeSeconds(0.0f)
{

}
const Profiler::AnalysisInformation & Profiler::getInfo( const char * name ) const
{
	static AnalysisInformation empty;
	AnalysisInformationMap::const_iterator i = m_analysisInformation.find(name);
	if (i==m_analysisInformation.end()) {
		return empty;
	} else {
		return (*i).second;
	}
}


float Profiler::getCurrentFps() const
{
	return m_fps;
}

void Profiler::dumpInfoToFile( const char * file, bool append /*= false*/ ) const
{
	std::fstream details;
	char* buf((char*)alloca(4096*sizeof(char)));
	details.open(file, std::ios_base::out | (append ? std::ios_base::app : std::ios_base::trunc));
	writeFormattedOutput(buf,4096);
	if (append) details << std::endl;
	details << "Profiling info for \"" << m_name << "\":" << std::endl;
	details << buf << std::endl;
	details.close();
}


Profilers * Profilers::m_instance = nullptr;

Profilers::Profilers()
{

}

Profiler* Profilers::getProfiler( const char * profilerName )
{
	// profilers gets instantiated when this method is first called
	Profilers * profilers = getInstance(); 
	ProfilersMap::const_iterator i = profilers->m_profilers.find(profilerName);
	if (i==profilers->m_profilers.end()) {
		DEBUG_PRINT("Profilers::getProfiler() - Creating profiler \"%s\"", profilerName);
		Profiler * p = new Profiler(profilerName);
		profilers->m_profilers[profilerName] = p;
		return p;
	}
	return (*i).second;
}

void Profilers::dumpToFile( const char * file )
{
#ifndef DISABLE_DIRECT_FILE_ACCESS
	Profilers * profilers = getInstance(); 
	ProfilersMap::const_iterator i = profilers->m_profilers.begin();
	bool append = false;
	for(;i!=profilers->m_profilers.end();i++) {
		(*i).second->dumpInfoToFile(file, append);
		append = true;
	}
#else
	INFO_PRINT("%s direct file access is disabled!", LOCATION);
#endif // VAGC_DISABLE_DIRECT_FILE_ACCESS
}

Profilers * Profilers::getInstance()
{
	if (m_instance == nullptr) 
	{
		m_instance = new Profilers();
	}
	return m_instance;
}

