#include <cstdlib>

#include "openglHelpers.h"
#include "GraphUtilities.h"

namespace opengl_helpers
{
	void drawGraph(graph_utilities::Graph* graph)
	{
		// graph nodes
		glColor3f(0.0f, 1.0f, 0.0f);
		glPointSize(3.0f);
		glBegin(GL_POINTS);
		for (auto n : graph->nodes)
		{
			glVertex3f(n->position[0], n->position[1], n->position[2]);
		}
		glEnd();

		// graph edges
		glLineWidth(1.0f);
		glBegin(GL_LINES);
		glColor3f(0.3f, 0.3f, 0.0f);
		for (auto e : graph->edges)
		{
			glVertex3f(e->source->position[0], e->source->position[1], e->source->position[2]);
			glVertex3f(e->destination->position[0], e->destination->position[1], e->destination->position[2]);
		}
		glEnd();
	}

	void drawBox(vec3 origin, vec3 halfDimension
		, GLfloat r, GLfloat g, GLfloat b, float lineWidth)
	{
		glPushMatrix();

		vec3 fv1, fv2, fv3, fv4, bv1, bv2, bv3, bv4;

		fv1 = origin - halfDimension;
		fv2 = { fv1[0] + 2.f * halfDimension[0], fv1[1], fv1[2] };
		fv3 = { fv2[0], fv1[1] + 2.f * halfDimension[1], fv1[2] };
		fv4 = { fv1[0], fv3[1], fv1[2] };

		bv1 = { fv1[0], fv1[1], fv1[2] + 2.f * halfDimension[2] };
		bv2 = { fv2[0], fv2[1], bv1[2] };
		bv3 = { fv3[0], fv3[1], bv1[2] };
		bv4 = { fv4[0], fv4[1], bv1[2] };

		glDisable(GL_LIGHTING);
		glLineWidth(lineWidth);
		glColor3f(r, g, b);
		glBegin(GL_LINE_LOOP);
		glVertex3f(fv1[0], fv1[1], fv1[2]);
		glVertex3f(fv2[0], fv2[1], fv2[2]);
		glVertex3f(fv3[0], fv3[1], fv3[2]);
		glVertex3f(fv4[0], fv4[1], fv4[2]);
		glVertex3f(fv1[0], fv1[1], fv1[2]);
		glEnd();
		glBegin(GL_LINE_LOOP);
		glVertex3f(bv1[0], bv1[1], bv1[2]);
		glVertex3f(bv2[0], bv2[1], bv2[2]);
		glVertex3f(bv3[0], bv3[1], bv3[2]);
		glVertex3f(bv4[0], bv4[1], bv4[2]);
		glVertex3f(bv1[0], bv1[1], bv1[2]);
		glEnd();
		glBegin(GL_LINES);
		glVertex3f(fv1[0], fv1[1], fv1[2]);
		glVertex3f(bv1[0], bv1[1], bv1[2]);

		glVertex3f(fv4[0], fv4[1], fv4[2]);
		glVertex3f(bv4[0], bv4[1], bv4[2]);

		glVertex3f(fv2[0], fv2[1], fv2[2]);
		glVertex3f(bv2[0], bv2[1], bv2[2]);

		glVertex3f(fv3[0], fv3[1], fv3[2]);
		glVertex3f(bv3[0], bv3[1], bv3[2]);
		glEnd();
		glPopMatrix();
	}
}