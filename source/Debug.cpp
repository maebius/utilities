#if defined(WIN32)

#ifdef _WIN32_WINNT
#undef _WIN32_WINNT
#endif
#define _WIN32_WINNT 0x0501 //define windows version to "Windows XP"

#ifdef WINVER
#undef WINVER
#endif
#define WINVER 0x0501 //define windows version to "Windows XP" 

#define WIN32_LEAN_AND_MEAN // use lean & mean headers - exclude rarely used stuff away!
#ifndef NOMINMAX
#define NOMINMAX // disable min() and max() defines
#endif
#include <windows.h>

#endif

#include <cstdarg>
#include <cstdio>

#include "Debug.h"
#include "Exception.h"

namespace Debug 
{
	bool g_printfAllOutput = false;

	void redirectOutputToStdout(bool outputToStdout)
	{
		g_printfAllOutput = outputToStdout;
	}

	void printStackUsage(const char* file, const char* function, int line)
	{
		unsigned long long usageBytes = 0;
		unsigned long long sizeBytes = 0;

#if defined(WIN32)
		// get the thread information block
		NT_TIB* pTib;
		__asm
		{
			mov EAX, FS:[18h]
				mov[pTib], EAX
		}

		// get the stack pointer
		PBYTE pPtr;
		_asm
		{
			mov pPtr, esp;
		}

		// calculate stack usage
		usageBytes = (unsigned long long)((PBYTE)pTib->StackBase - pPtr);

		// NOTE: sizeBytes is dynamic, as the system can enlarge the stack on demand.
		sizeBytes = (unsigned long long)pTib->StackBase - (unsigned long long)pTib->StackLimit;
#endif // WIN32

		Debug::printf("printStackUsage(): %s: %s(): line %d: %d bytes of stack used (of %d).\n"
			, file, function, line, (int)usageBytes, (int)sizeBytes);

		CUSTOM_ASSERT1(usageBytes <= sizeBytes);
	}

	void inline printDebug(const char * str)
	{
#if defined(WIN32)
		OutputDebugStringA(str);
#else
		std::printf("%s", str);
#endif 
	}

	void print(const char * formatstring, ...)
	{
		char buff[DEBUG_PRINTF_BUFSIZE];
		buff[0] = '\0';
		va_list args;
		va_start(args, formatstring);
		vsnprintf(buff, sizeof(buff) - 1, formatstring, args);
		va_end(args);

		if (g_printfAllOutput)
		{
			std::printf("%s\n", buff);
		}
		else
		{
			printDebug(buff);
			
			// conditionally output a newline
			size_t length = strlen(buff);
			if ((length>1 && buff[length - 1] != '\n') 
				&& !(length>1 && buff[length - 1] == ' ' 
				&& buff[length - 2] == ',')) 
			{
				printDebug("\n");
			}
		}
	}

	/* printing with no added newlines */
	void printf(const char * formatstring, ...)
	{
		char buff[DEBUG_PRINTF_BUFSIZE];
		memset(buff, 0, sizeof(buff));
		va_list args;
		va_start(args, formatstring);
		vsnprintf(buff, sizeof(buff) - 1, formatstring, args);
		va_end(args);

		if (g_printfAllOutput)
		{
			std::printf("%s", buff);
		}
		else
		{
			printDebug(buff);
		}
	}
};

