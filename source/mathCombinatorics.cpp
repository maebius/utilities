#include <iostream>
#include <cassert>
#include "mathCombinatorics.h"

namespace math_combinatorics
{
    using namespace std;

    void getFactorialDigits(long int n, std::vector<int>& result)
    {
        if (n <= 0)
            return;
        
        result.push_back(1);
     
        for (long i = 2; i <= n; i++)
        {
           int overflow = 0;
           for (long c = 0; c < result.size(); c++)
           {
               long value = result[c] * i + overflow;
               result[c] = value % 10;
               overflow = value / 10;
           }
           while (overflow > 0)
           {
               result.push_back(overflow % 10);
               overflow /= 10;
           }
       }
   }

   //! NOTE! Conversion from multi precision to long long int is not safe! Fails for big numbers.
    long long int getBinomialCoefficient(int k, int n)
    {
        assert(n >= k);
        //assert(n >= k && n <= 66);

        using namespace boost::multiprecision;

        if (n < 20)
        {
            using Precision = long long int;

            Precision fn = getFactorial<Precision>(n); 
            Precision fk = (n == k) ? fn : getFactorial<Precision>(k);
            Precision fnk = (n - k == n) ? fn : ((n - k == k) ? fk : getFactorial<Precision>(n-k));

            //std::cout << "fn: " << fn << ", fk: " << fk << ", fnk: " << fnk << std::endl;

            return  fn / (fk * fnk);
        }
        else if (n < 35)
        {
            // this can handle n = 34
            using Precision = checked_int128_t;

            Precision fn = getFactorial<Precision>(n); 
            Precision fk = (n == k) ? fn : getFactorial<Precision>(k);
            Precision fnk = (n - k == n) ? fn : ((n - k == k) ? fk : getFactorial<Precision>(n-k));

            //std::cout << "fn: " << fn << ", fk: " << fk << ", fnk: " << fnk << std::endl;

            return  (long long int) (fn / (fk * fnk));
        }
        else
        {
            using Precision = cpp_int;

            Precision fn = getFactorial<Precision>(n); 
            Precision fk = (n == k) ? fn : getFactorial<Precision>(k);
            Precision fnk = (n - k == n) ? fn : ((n - k == k) ? fk : getFactorial<Precision>(n-k));

            //std::cout << "fn: " << fn << ", fk: " << fk << ", fnk: " << fnk << std::endl;

            return  (long long int) (fn / (fk * fnk));
        }
    }
}