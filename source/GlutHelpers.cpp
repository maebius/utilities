
#include <cstdlib>
#include <cstdio>
#include <functional>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

namespace glut_helpers
{
	void reshape(int width, int height)
	{
		const double aspectRatio = (float)width / height;
		const double fieldOfView = 45.0;

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(fieldOfView, aspectRatio,
			1.0, 1000.0);  /* Znear and Zfar */
		glViewport(0, 0, width, height);
	}

	void init(
		void (*display) (void)
		, void (*handleKeyboard) (unsigned char, int, int)
		, void (*handleSpecialKeyboard) (int, int, int))
	{
		glutInitWindowSize(900, 600);
		glutInitWindowPosition(100, 100);
		glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
		
		int argc{1};
		char* argv {(char*)""};

		glutInit(&argc, &argv);

		glutCreateWindow("Training");
		glutDisplayFunc(display);
		glutReshapeFunc(glut_helpers::reshape);
		glutKeyboardFunc(handleKeyboard);
		glutSpecialFunc(handleSpecialKeyboard);

		glClearColor(0.1f, 0.1f, 0.1f, 1.f);

		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);    // Uses default lighting parameters

		glEnable(GL_DEPTH_TEST);

		glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
		glEnable(GL_NORMALIZE);

		// XXX docs say all polygons are emitted CCW, but tests show that some aren't.
		if (getenv("MODEL_IS_BROKEN"))
			glFrontFace(GL_CW);

		glColorMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE);

		glutGet(GLUT_ELAPSED_TIME);
		glutMainLoop();
	}
}