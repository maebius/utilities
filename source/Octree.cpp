
#include <vector>
#include "Octree.h"
#include "OctreeData.h"
#include "OctreeHelpers.h"

using namespace std;

Octree::Octree(Octree* parent, const vec3& origin, const vec3& halfDimension)
	: _parent(parent)
	, _origin(origin)
	, _halfDimension(halfDimension)
	, _data(nullptr)
{
	for (int i = 0; i < 8; ++i)
	{
		_children[i] = nullptr;
	}
}

Octree::~Octree() 
{
	for (int i = 0; i < 8; ++i)
	{
		delete _children[i];
	}
}

Octree* Octree::getParent()
{
	return _parent;
}

vector<Octree*> Octree::getChildren()
{
	vector<Octree*> result;
	for (int i = 0; i < 8; ++i)
	{
		if (_children[i] != nullptr)
		{
			result.push_back(_children[i]);
		}
	}
	return result;
}

Octree* Octree::getFirstWithData()
{
	if (isLeafNode())
		return this;

	for (int i = 0; i < 8; ++i)
	{
		auto child = _children[i]->getFirstWithData();
		if (child->getData() != nullptr)
		{
			return child;
		}
	}

	return nullptr;
}

OctreeData* Octree::getData()
{
	return _data;
}

int Octree::getFullyContainedDataCount()
{
	return _dataFullyContainedThisLevel.size();
}

OctreeData* Octree::getFullyContainedData(unsigned int index)
{
	assert(index < _dataFullyContainedThisLevel.size());

	return _dataFullyContainedThisLevel[index];
}

const vec3 Octree::getOrigin()
{
	return _origin;
}

const vec3 Octree::getHalfDimension()
{
	return _halfDimension;
}

int Octree::getOctantContainingPoint(const vec3* point) const
{
	// as we insert Data to our child nodes in spesific order,
	// we can just use bitfields for indexing
	int oct = 0;
	if ((*point)[0] >= _origin[0]) oct |= 4;
	if ((*point)[1] >= _origin[1]) oct |= 2;
	if ((*point)[2] >= _origin[2]) oct |= 1;
	return oct;
}

bool Octree::isLeafNode() const
{
	// leaf only if no children, and we either have all or none
	return _children[0] == nullptr;
}

void Octree::insert(OctreeData* data) 
{
	// if we have not divided this node yet ...
	if (isLeafNode()) 
	{
		// ... and there is no data, we can just assign as this will be our first Data
		if (_data == nullptr) 
		{
			_data = data;
		}
		else 
		{
			// we have not divided (leaf), but there is already data
			// -> we need to divide (split) this node to octants
			// -> after that we insert the new data to one of the octants
			// -> ... as well as the old data that was already here

			OctreeData *oldData = _data;
			_data = nullptr;

			// Split the current node and create new empty trees for each
			// child octant.

			// index pattern for child nodes: (i == child index, - == less than origin, + greater than origin)
			// 
			// i:	0 1 2 3 4 5 6 7
			// x:	- - - - + + + +
			// y:	- - + + - - + +
			// z:	- + - + - + - +
			
			for (int i = 0; i < 8; ++i) 
			{
				// new bounding box for the child
				vec3 newOrigin = _origin;
				newOrigin[0] += _halfDimension[0] * (i & 4 ? .5f : -.5f);
				newOrigin[1] += _halfDimension[1] * (i & 2 ? .5f : -.5f);
				newOrigin[2] += _halfDimension[2] * (i & 1 ? .5f : -.5f);
				_children[i] = new Octree(this, newOrigin, _halfDimension*.5f);
			}

			auto octantForOld = getOctantContainingPoint(oldData->getPosition());
			auto octantForNew = getOctantContainingPoint(data->getPosition());

			// check if the datas can be fully contained in their new nodes, if not, leave them here
			if (oldData->getPointCount() > 1
				&& !Octree::checkIfEnclosingFully(_children[octantForOld], oldData))
			{
				_dataFullyContainedThisLevel.push_back(oldData);
			}
			else
			{
				_children[octantForOld]->insert(oldData);
			}

			if (data->getPointCount() > 1
				&& !Octree::checkIfEnclosingFully(_children[octantForNew], data))
			{
				_dataFullyContainedThisLevel.push_back(data);
			}
			else
			{
				_children[octantForNew]->insert(data);
			}
		}
	}
	else 
	{
		// this node is already divided (not leaf)
		int octant = getOctantContainingPoint(data->getPosition());
		// we still need to check if the octant is able to hold the data fully, if not, put it here
		if (data->getPointCount() > 1
			&& !Octree::checkIfEnclosingFully(_children[octant], data))
		{
			_dataFullyContainedThisLevel.push_back(data);
		}
		else
		{
			_children[octant]->insert(data);
		}
	}
}

bool Octree::checkIfEnclosingFully(Octree* root, OctreeData *data)
{
	for (int i = 0; i < data->getPointCount(); i++)
	{
		if (!root->containsPoint(data->getPoint(i)))
			return false;
	}
	return true;
}

// This is a really simple routine for querying the tree for points
// within a bounding box defined by min/max points (bmin, bmax)
// All results are pushed into 'results'
void Octree::getPointsInsideBox(const vec3& bmin, const vec3& bmax, std::vector<Octree*>& results)
{
	// If we're at a leaf node, just see if the current data point is inside
	// the query bounding box
	if (isLeafNode()) 
	{
		if (_data != nullptr) 
		{
			vec3* p = _data->getPosition();
			if ((*p)[0] > bmax[0] || (*p)[1] > bmax[1] || (*p)[2] > bmax[2]) return;
			if ((*p)[0] < bmin[0] || (*p)[1] < bmin[1] || (*p)[2] < bmin[2]) return;
			results.push_back(this);
		}
	}
	else 
	{
		// We're at an interior node of the tree. We will check to see if
		// the query bounding box lies outside the octants of this node.
		for (int i = 0; i < 8; ++i) 
		{
			// Compute the min/max corners of this child octant
			vec3 cmax = _children[i]->_origin + _children[i]->_halfDimension;
			vec3 cmin = _children[i]->_origin - _children[i]->_halfDimension;

			// If the query rectangle is outside the child's bounding box, 
			// then continue
			if (cmax[0] < bmin[0] || cmax[1] < bmin[1] || cmax[2] < bmin[2]) continue;
			if (cmin[0] > bmax[0] || cmin[1] > bmax[1] || cmin[2] > bmax[2]) continue;

			// At this point, we've determined that this child is intersecting 
			// the query bounding box
			_children[i]->getPointsInsideBox(bmin, bmax, results);
		}
	}
}

bool Octree::containsPoint(const vec3* point)
{
	// does this volume contain the point at all?
	vec3 cmax = _origin + _halfDimension;
	vec3 cmin = _origin - _halfDimension;

	// if the point is outside our bounding box, we have no result
	if (cmax[0] < (*point)[0] || cmax[1] < (*point)[1] || cmax[2] < (*point)[2]) return false;
	if (cmin[0] > (*point)[0] || cmin[1] > (*point)[1] || cmin[2] > (*point)[2]) return false;

	return true;
}

Octree* Octree::getFirstChildForPoint(const vec3* point)
{
	if (!containsPoint(point) || isLeafNode())
		return nullptr;

	auto octant = getOctantContainingPoint(point);
	return _children[octant];
}

Octree* Octree::getLeafForPoint(const vec3* point)
{
	if (!containsPoint(point))
		return nullptr;

	// if we're at a leaf node, then we have already our result
	if (isLeafNode())
		return this;

	auto octant = getOctantContainingPoint(point);
	return _children[octant]->getLeafForPoint(point);
}


Octree* Octree::getFirstEnclosingNodeForPoint(const vec3* point) 
{
	if (!containsPoint(point))
		return nullptr;

	// if we're at a leaf node, then we have already our result
	if (isLeafNode())
		return this;

	// if we have data stored in this level, we also have our result here
	// -> note that we still might not be in leaf node
	if (_dataFullyContainedThisLevel.size() > 0)
		return this;

	auto octant = getOctantContainingPoint(point);
	_enclosingCell = _children[octant]->getFirstEnclosingNodeForPoint(point);
	return _enclosingCell;
}
