#include <cstdlib> // for abort()

// Uncomment to use only debugbreaks on error situations (NOT RECOMMENDED!!)
// #define ONLY_DEBUGBREAK_ON_ERROR

#include <cstdio>
#include <cstdarg>
#include <thread>
#include <chrono>

#include "Exception.h"
#include "Debug.h"

// Set to true to generate a software breakpoint when one of the
// Exception::throwError* methods get called. Will stop the execution 
// if debugger is running.
bool g_enableDebugBreak = true;
// Set to true to enter infinite loop instead of aborting, when
// one of the Exception::throwError* methods get called.
bool g_freezeOnError = true;

	
// the error handler callback - will be called at Exception::throwError().
void (*g_error_callback_func)(const char *location, const char * message) = nullptr;

void debug_break() 
{
	if (g_enableDebugBreak) 
	{
#if defined(WIN32)
	__debugbreak();
#endif
	}
}

void error_freeze() {
  if (g_freezeOnError) {
#ifndef ONLY_DEBUGBREAK_ON_ERROR
	int count = 60;
	ERROR_PRINT("Exception.cpp::error_freeze() - Meditating an error for 60 secs. Aborting program in a while.. If you are running a debugger you should break now.");
	debug_break();
	while (count > 0) {
		std::this_thread::sleep_for(std::chrono::seconds(1));
		count--;
	}
#else
	debug_break();
#endif
  } 
  ERROR_PRINT("Exception.cpp::error_freeze() - Aborting program.");
  abort();
}

#if defined(BOOST_NO_EXCEPTIONS)

/// Boost exception catch when C++ exceptions are disabled
#include <boost/throw_exception.hpp>
void boost::throw_exception(std::exception const & /*e*/) {
	ERROR_PRINT("Exception.cpp:boost::throw_exception() - Exception from boost!");
	error_freeze();
}
#endif

void Exception::setActionOnError( bool enableDebugBreak, bool freezeOnError )
{
	g_enableDebugBreak = enableDebugBreak;
	g_freezeOnError = freezeOnError;
}

void Exception::throwError( const char* location, const char * format, ... )
{
	char c[EXCEPTION_MESSAGE_MAX_LENGTH_CHARS];
	c[0] = '\0';
#ifndef CUSTOM_EXCEPTIONS_DISABLED
	try	{
#endif
	va_list params;
	va_start(params, format);       // params to point to the parameter list
	vsnprintf(c, EXCEPTION_MESSAGE_MAX_LENGTH_CHARS, format, params);	
	//vsprintf_s(c, EXCEPTION_MESSAGE_MAX_LENGTH_CHARS, format, params);
	va_end(params);
#ifndef CUSTOM_EXCEPTIONS_DISABLED
	}
	catch (...)
	{
		// just ignore if the construction of the message fails.
	}
#endif

	ERROR_PRINT("%s: %s", location, c);
	debug_break();
#ifndef CUSTOM_EXCEPTIONS_DISABLED
	// currently we throw only std::strings
	throw std::string(location) + ": " + std::string(c);
#else
	if (g_error_callback_func!=NULL) g_error_callback_func(location, c);
	error_freeze();
#endif 

}

void Exception::throwError( const char * file, int line, const char * format, ... )
{
	char c[EXCEPTION_MESSAGE_MAX_LENGTH_CHARS];
	//sprintf_s(c, "%s(%i): ", file, line);
	sprintf(c, "%s(%i): ", file, line);
	
	char c2[EXCEPTION_MESSAGE_MAX_LENGTH_CHARS];
	c2[0] = '\0';
#ifndef CUSTOM_EXCEPTIONS_DISABLED
	try	{
#endif
	va_list params;
	va_start(params, format);       // params to point to the parameter list
	//vsprintf_s(c2, EXCEPTION_MESSAGE_MAX_LENGTH_CHARS, format, params);
	vsnprintf(c2, EXCEPTION_MESSAGE_MAX_LENGTH_CHARS, format, params);
	va_end(params);
#ifndef CUSTOM_EXCEPTIONS_DISABLED
	}
	catch (...)
	{
		// just ignore if the construction of the message fails.
	}
#endif
	ERROR_PRINT("%s%s", c, c2);
	debug_break();
#ifndef CUSTOM_EXCEPTIONS_DISABLED
	// currently we throw only std::strings
	throw std::string(c) + std::string(c2);
#else
	if (g_error_callback_func!=NULL) g_error_callback_func(c, c2);
	error_freeze();
#endif
}

void Exception::throwErrorIf( bool failure, const char* location, const char * format, ... )
{
	// if all is ok, we can leave immediately.
	if (!failure) { 
		return;
	} else {
		char c[EXCEPTION_MESSAGE_MAX_LENGTH_CHARS];
		c[0] = '\0';
#ifndef CUSTOM_EXCEPTIONS_DISABLED
		try	{
#endif
			va_list params;
			va_start(params, format);       // params to point to the parameter list
			//vsprintf_s(c, EXCEPTION_MESSAGE_MAX_LENGTH_CHARS, format, params);
			vsnprintf(c, EXCEPTION_MESSAGE_MAX_LENGTH_CHARS, format, params);
			va_end(params);
#ifndef CUSTOM_EXCEPTIONS_DISABLED
		}
		catch (...)
		{
			// just ignore if the construction of the message fails.
		}
#endif

		ERROR_PRINT("%s: %s", location, c);
		debug_break();
#ifndef VAGC_EXCEPTIONS_DISABLED
		// currently we throw only std::strings
		throw std::string(location) + ": " + std::string(c);
#else
		if (g_error_callback_func!=NULL) g_error_callback_func(location, c);
		error_freeze();
#endif
	}
}


#ifndef VAGC_EXCEPTIONS_DISABLED

Exception::Exception() /*throw()*/
{

}

Exception::Exception( const std::string & errormsg ) : error(errormsg)
{

}

Exception::Exception( const char *formatstr, ... )
{
	try
	{
		char c[EXCEPTION_MESSAGE_MAX_LENGTH_CHARS];
		va_list params;
		va_start(params, formatstr);       // params to point to the parameter list
		//vsprintf_s(c, EXCEPTION_MESSAGE_MAX_LENGTH_CHARS, formatstr, params);
		vsnprintf(c, EXCEPTION_MESSAGE_MAX_LENGTH_CHARS, formatstr, params);
		va_end(params);
		error = c;
	}
	catch (...)
	{
		// just ignore if the construction of the message fails.
	}
}
Exception::~Exception() /*throw()*/
{

}

std::string Exception::str() const /*throw()*/
{
	return error;
}

const char* Exception::what() const throw()
{
	return error.c_str();
}

#else

void Exception::setErrorHandler( void (*error_callback_func)(const char *location, const char * message) )
{
	g_error_callback_func = error_callback_func;
}

#endif 