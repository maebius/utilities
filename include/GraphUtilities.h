#pragma once

#include <vector>
#include <functional>
#include <memory>

#include "VectorDef.h"
#include "priorityQueue.h"
#include "PrintHelpers.h"

namespace graph_utilities
{
	struct Node
	{
		vec3	position;
		int		value; //! should be more general, class etc.
	};

	struct Edge
	{
		Node* source;
		Node* destination;
		float cost;
	};

	struct Graph
	{
		//! Nodes for the graph. NOTE! Graph takes the ownership and deletes these as well. 
		std::vector<Node*> nodes;
		//! Edges for the graph. NOTE! Graph takes the ownership and deletes these as well. 
		std::vector<Edge*> edges;

		~Graph();
		std::vector<Edge*>	getOutgoingEdges(const Node* node);
		Edge*				getEdge(const Node* src, const Node* trg);
	};

	struct Record
	{
		Node* node;
		Edge* edge;
		float costSoFar;
		float estimatedTotalCost;
	};

	inline bool operator< (const Record& lhs, const Record& rhs)
	{
		return lhs.estimatedTotalCost < rhs.estimatedTotalCost; 
	}
	inline bool operator> (const Record& lhs, const Record& rhs){ return rhs < lhs; }
	inline bool operator<=(const Record& lhs, const Record& rhs){ return !(lhs > rhs); }
	inline bool operator>=(const Record& lhs, const Record& rhs){ return !(lhs < rhs); }

	enum class SearchState
	{
		UnInitialised, Running, Success, Fail
	};

	using Goals = std::vector<Node*>;

	using Records = std::vector<Record*>;
	using RecIte = Records::iterator;

	using OrderedRecords = sort_utilities::PriorityQueue<Record*>;
	
	using Heurestic = std::function < float(const Graph*, const Node*, const Node*) >;

	RecIte getRecord(Node* node, Records& records);
	RecIte getRecord(Node* node, OrderedRecords& records);

	Record* getMostPromising(Records& records);
	Record* getMostPromising(OrderedRecords& records);

	Graph* createRandomGraph(
		const int positionCount
		, const int nodeCount
		, const vec3& boundingOrigin
		, const vec3& halfDimension
		, const bool biDirectional = true);

	
	Graph* loadTriangleGraphFromFile(const std::string& filename);

};
