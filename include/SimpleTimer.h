#ifndef _SIMPLE_TIMER_H_
#define _SIMPLE_TIMER_H_

#include <chrono>

#if _MSC_VER && _MSC_VER > 1800
#define CHECK_TEMPLATE_ARGUMENT
#endif 

namespace time_helpers
{
#ifdef CHECK_TEMPLATE_ARGUMENT
	/*! Helper to restrict SimpleTimer static_assert to actual std::chrono::duration 
	source: http://stackoverflow.com/questions/28875853/how-can-i-use-stdchronoduration-as-a-template-parameter
	*/
	template <typename T>
	struct is_chrono_duration
	{		
		static constexpr bool value = false;
	};

	/*! Helper to restrict SimpleTimer static_assert to actual std::chrono::duration
	source: http://stackoverflow.com/questions/28875853/how-can-i-use-stdchronoduration-as-a-template-parameter
	*/
	template <typename Rep, typename Period>
	struct is_chrono_duration<std::chrono::duration<Rep, Period>>
	{
		static constexpr bool value = true;
	};

#endif // CHECK_TEMPLATE_ARGUMENT
	template <typename Duration = std::chrono::milliseconds>
	class SimpleTimer
	{
#ifdef CHECK_TEMPLATE_ARGUMENT
		static_assert(is_chrono_duration<Duration>::value, "duration must be a std::chrono::duration");
#endif // CHECK_TEMPLATE_ARGUMENT

	public:
		SimpleTimer()
		{
			restart();
		}

		long long getSinceEpoch()
		{
			auto result = _start.time_since_epoch();
			return std::chrono::duration_cast<Duration>(result).count();
		}

		long long getElapsed()
		{
			auto now = std::chrono::high_resolution_clock::now();
			auto result = now - _start;
			return std::chrono::duration_cast<Duration>(result).count();
		}

		void restart()
		{
			_start = std::chrono::high_resolution_clock::now();
		}

	private:
		std::chrono::time_point<std::chrono::high_resolution_clock> _start;
	};
};

#endif // _SIMPLE_TIMER_H_
