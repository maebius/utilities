#ifndef _GLUT_HELPERS_H_
#define _GLUT_HELPERS_H_

namespace glut_helpers
{
	void reshape(int width, int height);

	void init(
		void (*display) (void)
		, void (*handleKeyboard) (unsigned char, int, int)
		, void (*handleSpecialKeyboard) (int, int, int));

}

#endif // _GLUT_HELPERS_H_
