#ifndef _TRIANGLE_H_
#define _TRIANGLE_H_

#include "VectorDef.h"

struct Triangle
{
	explicit Triangle(vec3 p1, vec3 p2, vec3 p3);

	vec3 p[3];

	vec3 centroid;
};

namespace triangle_helpers
{
	vec3 calculateCentroid(vec3* a, vec3* b, vec3* c);
	vec3 getClosestPoint(Triangle* triangle, const vec3& point);
};

#endif // _TRIANGLE_H_