#ifndef _OPENGL_INCLUDE_H_
#define _OPENGL_INCLUDE_H_

#include <cstdlib>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#endif // _OPENGL_INCLUDE_H_
