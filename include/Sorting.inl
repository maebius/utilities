
#include <vector>
#include <random>
#include <iostream>
#include <functional>
#include <memory>
#include <iterator>

namespace sort_utilities
{
	using namespace std;

	using uint = unsigned int;

	/*!
		Basic insertion sort, where result is arranged directly to the input vector.
	*/
	template<typename T>
	void insertionSort(vector<T>* input, function<bool(T, T)> sortCriteria)
	{
		// start by ordering the first two in sequence (i == 0,1)
		for (uint i = 1; i < input->size(); i++)
		{
			// take the current element
			auto key = (*input)[i];
			int j = i - 1;
			// ... and as long as it satisfies the sortCriteria to previous value,
			// go backward in finding the proper spot
			while (j > -1 && sortCriteria(key, (*input)[j]))
			{
				// need to move the previous value up, as the current value
				// satisfied the sortCriteria and should be before
				(*input)[j + 1] = (*input)[j];
				// investigate previous
				j--;
			}
			// now we have found the correct position for current
			(*input)[j + 1] = key;
		}
	}

	/*!
		Insertion sort where input is left as it was and result constructed to new vector.
	*/
	template<typename T>
	vector<T> insertionSortCopy(vector<T>* input, function<bool(T, T)> sortCriteria)
	{
		vector<T> result;
		copy(input->begin(), input->end(), back_inserter(result));
		insertionSort<T>(&result, sortCriteria);
		return result;
	}

	template<typename T>
	void bubbleSort(vector<T>* input, function<bool(T, T)> sortCriteria)
	{
		// for every position in array, find the correct place
		// by comparing against every other position that are still not handled
		for (uint i = 0; i < input->size(); i++)
		{
			// need to go down to the position that is not handled,
			// as we "bubble" down the right answer to that and on next
			// iteration positions up to that are final
			for (uint j = input->size() - 1; j > i; j--)
			{
				auto second = (*input)[j];
				auto first = (*input)[j-1];

				// swap?
				if (sortCriteria(second, first))
				{
					(*input)[j] = first;
					(*input)[j - 1] = second;
				}
			}
		}
	}

	template<typename T>
	vector<T> bubbleSortCopy(vector<T>* input, function<bool(T, T)> sortCriteria)
	{
		vector<T> result;
		copy(input->begin(), input->end(), back_inserter(result));
		bubbleSort<T>(&result, sortCriteria);
		return result;
	}

	template<typename T>
	void selectionSort(vector<T>* input, function<bool(T, T)> sortCriteria)
	{
		// go through every element and "directly select" a correct position for them
		for (uint i = 0; i < input->size(); i++)
		{
			auto selection = (*input)[i];
			// the initial correct place for this is the original position
			int selectionIndex = i;
			// compare to all that come after
			// (as we start from 0, all before are already in correct places)
			for (uint j = i + 1; j < input->size(); j++)
			{
				// if the current selection does not satisfy sortCriteria with the current value,
				// we need to update the current selection
				if (sortCriteria((*input)[j], selection))
				{
					selection = (*input)[j];
					selectionIndex = j;
				}
			}
			// we have now gone through all remaining positions, so our selection is done, swap
			(*input)[selectionIndex] = (*input)[i];
			(*input)[i] = selection;
		}
	}

	template<typename T>
	vector<T> selectionSortCopy(vector<T>* input, function<bool(T, T)> sortCriteria)
	{
		vector<T> result;
		copy(input->begin(), input->end(), back_inserter(result));
		selectionSort<T>(&result, sortCriteria);
		return result;
	}
}