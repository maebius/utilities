
#pragma once

#include <vector>
#include <random>
#include <iostream>
#include <functional>
#include <memory>
#include <iterator>

namespace sort_utilities
{
	using namespace std;

	template<typename T>
	void heapify(vector<T>* input, function<bool(const T, const T)> sortCriteria, int heapSize, int index)
	{
		auto leftIndex = (index << 1) + 1;
		auto rightIndex = leftIndex + 1;

		auto largestIndex = index;

		// NOTE! we want the interface to conform on how other searches work,
		// so if the sortCriteria is for example "<", we get the biggest on the root
		// -> as heapSort returns root last, smallest comes first
		// -> the other way around with ">", we build min-heap where smallest is on root, comes last

		if (leftIndex < heapSize
			&& !sortCriteria((*input)[leftIndex], (*input)[index]))
		{
			largestIndex = leftIndex;
		}
		if (rightIndex < heapSize
			&& !sortCriteria((*input)[rightIndex], (*input)[largestIndex]))
		{
			largestIndex = rightIndex;
		}
		if (largestIndex != index)
		{
			auto temp = (*input)[index];
			(*input)[index] = (*input)[largestIndex];
			(*input)[largestIndex] = temp;
			heapify<T>(input, sortCriteria, heapSize, largestIndex);
		}
	}

	template<typename T>
	void buildHeap(vector<T>* input, function<bool(const T, const T)> sortCriteria)
	{
		int heapSize = static_cast<int> (input->size());
		for (int i = (heapSize / 2) - 1; i > -1; i--)
		{
			heapify<T>(input, sortCriteria, heapSize, i);
		}
	}

	template<typename T>
	void heapSort(vector<T>* input, function<bool(const T, const T)> sortCriteria)
	{
		buildHeap(input, sortCriteria);
		int heapSize = static_cast<int> (input->size());
		for (int i = input->size() - 1; i > 0; i--)
		{
			auto temp = (*input)[0];
			(*input)[0] = (*input)[i];
			(*input)[i] = temp;
			heapSize--;
			heapify<T>(input, sortCriteria, heapSize, 0);
		}
	}
}