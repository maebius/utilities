
#include <vector>
#include <random>
#include <iostream>
#include <functional>
#include <memory>
#include <iterator>

namespace sort_utilities
{
	using namespace std;
	using uint = unsigned int;

	template<typename T>
	void mergeSortCombine(vector<T>* input
		, function<bool(T, T)> sortCriteria
		, uint startIndex
		, uint stopIndex
		, uint middleIndex)
	{
		// merge sub results, we sadly cannot do it in place
		auto currentLow = startIndex;
		auto currentHigh = middleIndex + 1;
		vector<T> result;

		// obviously, need to handle the whole range
		for (uint i = startIndex; i <= stopIndex; i++)
		{
			// only consider the lower bracket if there is still elements
			// ... and select first element of lower bracket if nothing left in higher bracket
			// or the current in lower satisfies sortCriteria against current in high
			if (currentLow < middleIndex + 1
				&& (currentHigh > stopIndex
				|| sortCriteria((*input)[currentLow], (*input)[currentHigh])))
			{
				result.push_back((*input)[currentLow]);
				currentLow++;
			}
			else
			{
				// we are here because the element in higher bracket was our choice
				result.push_back((*input)[currentHigh]);
				currentHigh++;
			}
		}
		// write output to the input vector
		for (uint i = startIndex, j = 0; i <= stopIndex; i++, j++)
		{
			(*input)[i] = result[j];
		}
	}

	template<typename T>
	void mergeSortRange(vector<T>* input, function<bool(T, T)> sortCriteria, uint startIndex, uint stopIndex)
	{
		// only one element on this bracket, so it is "already in order"
		if (startIndex == stopIndex)
			return;

		// recurse
		auto middleIndex = (startIndex + stopIndex) / 2;
		mergeSortRange(input, sortCriteria, startIndex, middleIndex);
		mergeSortRange(input, sortCriteria, middleIndex + 1, stopIndex);
	}

	template<typename T>
	void mergeSort(vector<T>* input, function<bool(T, T)> sortCriteria)
	{
		mergeSortRange(input, sortCriteria, 0, input->size() - 1);
	}
}