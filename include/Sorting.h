#ifndef _SORTING_H_
#define _SORTING_H_

#include <vector>
#include <functional>

namespace sort_utilities
{
	template<typename T>
	void insertionSort(std::vector<T>* input, std::function<bool(T, T)> sortCriteria);
	
	template<typename T>
	std::vector<T> insertionSortCopy(std::vector<T>* input, std::function<bool(T, T)> sortCriteria);

	template<typename T>
	void bubbleSort(std::vector<T>* input, std::function<bool(T, T)> sortCriteria);

	template<typename T>
	std::vector<T> bubbleSortCopy(std::vector<T>* input, std::function<bool(T, T)> sortCriteria);

	template<typename T>
	void selectionSort(std::vector<T>* input, std::function<bool(T, T)> sortCriteria);

	template<typename T>
	std::vector<T> selectionSortCopy(std::vector<T>* input, std::function<bool(T, T)> sortCriteria);

	template<typename T>
	void mergeSortRange(std::vector<T>* input, std::function<bool(T, T)> sortCriteria, unsigned int start, unsigned int stop);

	template<typename T>
	void mergeSort(std::vector<T>* input, std::function<bool(T, T)> sortCriteria);

}

#include "Sorting.inl"

#endif // _SORTING_H_
