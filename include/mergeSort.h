#ifndef _MERGE_SORT_H_
#define _MERGE_SORT_H_

#include <vector>
#include <functional>

namespace sort_utilities
{
	template<typename T>
	void mergeSortRange(std::vector<T>* input, std::function<bool(T, T)> sortCriteria, unsigned int start, unsigned int stop);

	template<typename T>
	void mergeSort(std::vector<T>* input, std::function<bool(T, T)> sortCriteria);
}

#include "mergeSort.inl"

#endif // _MERGE_SORT_H_
