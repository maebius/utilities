#pragma once

#include "GraphUtilities.h"

namespace graph_search
{
	using namespace graph_utilities;

	class IAStar
	{
	public:
		/*!
			Initializes our search query. This needs to be called before executing the search.
		*/
		virtual void initializeSearch(Node* start, Goals goals) = 0;

		/*!
			Executes the search. Returns true if the search finished, false if it still unfinished and needs to
			be called again.
		*/
		virtual bool execute(int nodesPerIteration = 0) = 0;

		/*!
			Gets current status of the search.
		*/
		virtual SearchState getState() = 0;

		/*!
			Gets the constructed path so far. Returns a complete path when getState returns success.
		*/
		virtual std::vector<Edge*> getResult() = 0;
	};
}