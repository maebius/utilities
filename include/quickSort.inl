
#include <vector>
#include <random>
#include <iostream>
#include <functional>
#include <memory>
#include <iterator>

namespace sort_utilities
{
	using namespace std;

	template<typename T>
	int quickSortPartition(vector<T>* input, function<bool(T, T)> sortCriteria, int start, int last)
	{
		// idea of the whole thing is that we maintain 3 partitions for the input values,
		// and that these partitions are defined by the indices "i" and "j"
		// -> we handle values one by one, and if the current satisfies the sortCriteria,
		// we exchange that and the first that did not (pointed by "i+1")
		// -> the "i" is updated to current when this exchange happens (otherwise left untouched), thus
		// making sure that all handled values before "i" are of those that satisfied the sortCriteria
		// -> so, "i" points to the value that was last swapped (and "i+1" to value that will be swapped)
		// -> as a last thing, we swap the pivot to position (i+1), so we have before that the elements
		// that should come before, and after that all the ones that we kind of left "untouched"
		// (sure, the first of that group was always exchanged with a value that filled the sortCriteria)

		int i = start - 1;
		// the last element here is the pivot where we 
		T p = (*input)[last];
		for (int j = start; j < last; j++)
		{
			// as long as the currently investigated element
			// 
			T current = (*input)[j];
			if (sortCriteria(current, p))
			{
				i++;
				T a = (*input)[i];
				(*input)[i] = current;
				(*input)[j] = a;
			}
		}

		i++;
		T a = (*input)[i];
		(*input)[i] = p;
		(*input)[last] = a;

		return i;
	}

	template<typename T>
	int quickSortRandomizedPartition(vector<T>* input, function<bool(T, T)> sortCriteria, int start, int last)
	{
		// take randomly an element and switch that with a last,
		// that is used as a pivot
		int q = random_helpers::getRandom(start, last);
		T a = (*input)[q];
		(*input)[q] = (*input)[last];
		(*input)[last] = a;
		return quickSortPartition(input, sortCriteria, start, last);
	}

	template<typename T>
	void quickSortRange(vector<T>* input, function<bool(T, T)> sortCriteria, int start, int end)
	{
		if (start >= end)
			return;

		auto q = quickSortPartition(input, sortCriteria, start, end);
		quickSortRange(input, sortCriteria, start, q - 1);
		quickSortRange(input, sortCriteria, q + 1, end);
	}


	template<typename T>
	void quickSort(vector<T>* input, function<bool(T, T)> sortCriteria)
	{
		quickSortRange(input, sortCriteria, 0, input->size() - 1);
	}

	template<typename T>
	void quickSortRandomizedRange(vector<T>* input, function<bool(T, T)> sortCriteria, int start, int last)
	{
		if (start >= last)
			return;

		auto q = quickSortRandomizedPartition(input, sortCriteria, start, last);
		quickSortRandomizedRange(input, sortCriteria, start, q - 1);
		quickSortRandomizedRange(input, sortCriteria, q + 1, last);
	}

	template<typename T>
	void quickSortRandomized(vector<T>* input, function<bool(T, T)> sortCriteria)
	{
		quickSortRandomizedRange(input, sortCriteria, 0, input->size() - 1);
	}
}