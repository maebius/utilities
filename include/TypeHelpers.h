#ifndef _TypeHelpers_h
#define _TypeHelpers_h

#include <string>
#include <vector>

#include <random>
#include <sstream>

#include <iomanip>
#include <ctime>

template <typename T>
inline std::string to_string(T value)
{
	std::ostringstream os;
	os << value;
	return os.str();
}

template <typename T>
inline std::string to_string(T value, int precision)
{
	std::ostringstream os;
	os << std::fixed << std::showpoint;
	os << std::setprecision(precision);
	os << value;
	return os.str();
}

// this to get around the fact that [] does not work for const maps (so, neither in const methods for member maps)
template <typename T>
typename T::mapped_type get(T const& map, typename T::key_type const& key)
{
	typename T::const_iterator iter(map.find(key));
	return iter != map.end() ? iter->second : typename T::mapped_type();
}

using double_dist = std::uniform_real_distribution<double>;
using int_dist = std::uniform_int_distribution<int>;

namespace TypeHelpers
{
	const std::string defaultTimeFormat{ "%Y-%b-%d %H:%M:%S" };

	std::string getSuffix(int);

	std::time_t getCurrentTime();
	std::time_t getTime(std::string, std::string = defaultTimeFormat);
	std::string getTime(std::time_t, std::string = defaultTimeFormat);
}


#endif // MathHour_TypeHelpers_h
