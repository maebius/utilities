#ifndef _RANDOM_HELPERS_H_
#define _RANDOM_HELPERS_H_

#include <vector>

namespace random_helpers
{
	int getRandom(int min, int max);
	double getRandom(double min, double max);

	void fill(int min, int max, int count, std::vector<long long>& result);
	
}

#endif // _RANDOM_HELPERS_H_