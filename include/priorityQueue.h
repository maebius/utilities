#pragma once

#include <vector>
#include <functional>

namespace sort_utilities
{
	template<typename T>
	class PriorityQueue
	{
	public:
		using TVec = std::vector<T>;
		using TVecIte = typename TVec::iterator;

		explicit PriorityQueue(TVec* input, std::function<bool(const T, const T)> sortCriteria);
		
		~PriorityQueue();

		unsigned int size() const;
		bool empty() const;

		T& top() const;
		T pop();

		TVec & getData() { return *_data; }
		TVecIte begin() { return _data->begin(); }
		TVecIte end() { return _data->end(); }
		
		void push_back(T value);
		void erase(TVecIte value);

		void clear();

	private:
		void increaseKey(int index);

		std::function<bool(const T, const T)> _sortCriteria;
		TVec* _data;
		bool  _ownsData;
	};
};

#include "priorityQueue.inl"

