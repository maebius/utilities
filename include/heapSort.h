#pragma once

#include <vector>
#include <functional>

namespace sort_utilities
{
	template<typename T>
	void heapify(std::vector<T>* input, std::function<bool(const T, const T)> sortCriteria, unsigned int index);

	template<typename T>
	void buildHeap(std::vector<T>* input, std::function<bool(const T, const T)> sortCriteria);

	template<typename T>
	void heapSort(std::vector<T>* input, std::function<bool(const T, const T)> sortCriteria);
}

#include "heapSort.inl"
