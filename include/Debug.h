
#ifndef _DEBUG_H_
#define _DEBUG_H_


#ifdef SHOW_FUNC_CALLS
// Macros that can be used to debug TTY/Console output labels inside functions.
#if defined(WIN32)
#define FUNC_ENTER INFO_PRINT(LOCATION " %s - enter", __FUNCTION__);
#define FUNC_CALLED INFO_PRINT(LOCATION " %s - called", __FUNCTION__);
#define FUNC_EXIT INFO_PRINT(LOCATION " %s - exit", __FUNCTION__);
#else
#define FUNC_ENTER INFO_PRINT(LOCATION " %s - enter", __PRETTY_FUNCTION__);
#define FUNC_CALLED INFO_PRINT(LOCATION " %s - called", __PRETTY_FUNCTION__);
#define FUNC_EXIT INFO_PRINT(LOCATION " %s - exit", __PRETTY_FUNCTION__);
#endif // defined(WIN32)
#else 
#define FUNC_ENTER 
#define FUNC_CALLED 
#define FUNC_EXIT
#endif


/**
* Cross-platform Debug output. Has a printf method for outputting debug information
* depending on the platform.
*
* You should use DEBUG_PRINT -macros defined below:
* \code
DEBUG_PRINT("Class::method() - Static message");
DEBUG_PRINT("GameApplication::configure() - GL RenderSystem Config Option: %s ", (*i).first.c_str());
DEBUG_PRINT("GameData::changeGameState() - State transition from %s to %s", prevGamestate.c_str(), currentGameState.c_str());
* \endcode
*
*/
namespace Debug 
{

	static const unsigned int DEBUG_PRINTF_BUFSIZE = 2048;

	/**
	* Debug outputting method, follows standard printf conventions, except automatically appends a line break. (Wonder why...)
	* For example:
	* \code
	Debug::print("GameApplication::configure() - GL RenderSystem Config Option: %s", configoption.c_str());
	* \endcode
	* \note You should use DEBUG_PRINT -macros to access these methods!
	*
	* In normal cases you don't need to put a newline end of the string (a newline is added automatically if it does not exist). An exception to this:
	* DEBUG_PRINT and ERROR_PRINT support for printing several items into one line. Newline is NOT being added
	* if the string to be outputted ends with ", " (without newline).
	*/
	//lint -printf(1, VAGC::Debug::print)
	void print(const char * format, ...);

	/**
	* Debug outputting method, follows standard printf conventions.
	*/
	void printf(const char * format, ...);

	/**
	* Print the amount of stack we are using at the moment. Platform-specific
	* implementation - may not be available on all platforms & configurations.
	*/
	void printStackUsage(const char* file, const char* function, int line);

	/**
	* Direct Debug::print* output either to debugger (using OutputDebugString) or to
	* standard output stream (STDOUT) (using standard printf). Default is false - by default
	* we output only to the debugger.
	*/
	void redirectOutputToStdout(bool outputToStdout);
};


// Define some handy error output macros (see for usage instructions in Debug class).
// On most platforms we can implement INFO_PRINT and ERROR_PRINT using Debug::print().

#if !defined(CONFIG_SHIPPING) || defined(FORCE_DEBUG_OUTPUT)
#define INFO_PRINT(fmt, ...) Debug::print(fmt, ##__VA_ARGS__)
#define INFO_PRINTF(fmt, ...) Debug::printf(fmt, ##__VA_ARGS__)
#else
#define INFO_PRINT(x, ...) 
#define INFO_PRINTF(x, ...) 
#endif

#define ERROR_PRINT(fmt, ...) Debug::print(fmt, ##__VA_ARGS__)
#define ERROR_PRINTF(fmt, ...) Debug::printf(fmt, ##__VA_ARGS__)


// Define some handy debug output macros
// that will be compiled out with release build
#if (defined(_DEBUG) || defined (FORCE_DEBUG_OUTPUT)) && !defined(DISABLE_DEBUG_OUTPUT)

#define DEBUG_PRINT(fmt, ...) Debug::print(fmt, ##__VA_ARGS__)
#define DEBUG_PRINTF(fmt, ...) Debug::printf(fmt, ##__VA_ARGS__)
#else
#define DEBUG_PRINT(x, ...) 
#define DEBUG_PRINTF(x, ...) 
#endif

#endif // _DEBUG_H_
