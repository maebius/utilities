#ifndef _QUICK_SORT_H_
#define _QUICK_SORT_H_

#include <vector>
#include <functional>

namespace sort_utilities
{
	template<typename T>
	void quickSortRange(
		std::vector<T>* input
		, std::function<bool(T, T)> sortCriteria
		, int start
		, int end);

	template<typename T>
	void quickSortRandomizedRange(
		std::vector<T>* input
		, std::function<bool(T, T)> sortCriteria
		, int start
		, int end);

	template<typename T>
	void quickSort(
		std::vector<T>* input
		, std::function<bool(T, T)> sortCriteria);

	template<typename T>
	void quickSortRandomized(std::vector<T>* input
		, std::function<bool(T, T)> sortCriteria);

}

#include "quickSort.inl"

#endif // _QUICK_SORT_H_
