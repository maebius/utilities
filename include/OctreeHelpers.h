#ifndef _OctreeHelpers_H
#define _OctreeHelpers_H

#include <vector>

#include "VectorDef.h"

struct Triangle;
class Octree;
class OctreeData;

/*! \namespace OctreeHelpers
	\brief Helper functions to deal with Octree class
*/
namespace OctreeHelpers
{
	/*!
		Construct an Octree from the given points.

		\param vertices All the vertex points we want to include in our Octree.
		\param origin The geometric center of the given vertices
		\param halfDimensions Half of the bounding box dimensions enclosing all the input vertices.
	*/
	Octree* makeVertexOctree(std::vector<vec3>& vertices, vec3 origin, vec3 halfDimensions);

	/*!
	Construct an Octree from the given triangles.

	\param vertices All the vertex points we want to include in our Octree.
	\param origin The geometric center of the given vertices
	\param halfDimensions Half of the bounding box dimensions enclosing all the input vertices.
	*/
	Octree* makeTriangleOctree(std::vector<Triangle>& triangles, vec3 origin, vec3 halfDimensions);

	/*!
		Returns the node that is closest the given point.

		\param octree The Octree datastructure to where we do the query
		\param point The point that we try to locate in our Octree
		\param projectIfOutside If the point is outside the octree bounding volume, we project the point to it and continue if this is true.
	*/
	Octree* getClosestLeafWithData(Octree* octree, const vec3& point, bool projectIfOutside = true);


	/*!
	Returns the data from octree that is closest the given point.

	\param octree The Octree datastructure to where we do the query
	\param point The point that we try to locate in our Octree
	*/
	void getClosestData(Octree* octree
		, const vec3& point
		, Octree** outRootCell
		, Octree** outCell
		, OctreeData** outData
		, bool projectIfOutside = true);

};

#endif // _OctreeHelpers_H