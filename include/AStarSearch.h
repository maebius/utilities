#pragma once

#include "GraphUtilities.h"
#include "IAStar.h"

namespace graph_search
{
	using namespace graph_utilities;

	class AStarSearch : public IAStar
	{
	public:
		AStarSearch(Graph* graph, Heurestic h = nullptr);
		~AStarSearch(); // because of using Pimpl - idiom, need decl of destructor here (not definition!)

		/*!
			Initializes our search query. This needs to be called before executing the search.
		*/
		void initializeSearch(Node* start, Goals goals) override;

		/*!
			Executes the search. Returns true if the search finished, false if it still unfinished and needs to
			be called again.
		*/
		bool execute(int nodesPerIteration = 0) override;

		/*!
			Gets current status of the search.
		*/
		SearchState getState() override;

		/*!
			Gets the constructed path so far. Returns a complete path when getState returns success.
		*/
		std::vector<Edge*> getResult() override;

	private:
		/*!
			Is the given node one of the goal nodes?
		*/
		bool isGoalNode(const Node* node);

		/*!
			Get the heurestic cost to given node
		*/
		float getLowestHeuresticCost(const Node* endNode);

		/*!
			We use Pimpl-idiom to store our AStar search member data.
		*/
		struct Impl;
		std::unique_ptr<Impl> _pimpl;
	};
}