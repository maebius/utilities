#ifndef _PRINT_HELPERS_H_
#define _PRINT_HELPERS_H_

#include <vector>

namespace print_helpers
{
	template<typename T>
	void printVector(const std::vector<T>& input, const std::string & format);

	template<typename T>
	void printQueueByPopping(T& q, const std::string & format);
}

#include "PrintHelpers.inl"

#endif // _PRINT_HELPERS_H_
