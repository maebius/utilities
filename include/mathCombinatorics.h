#pragma once

#include <cassert>
#include <iostream>
#include <vector>
#include <set>

#include <boost/multiprecision/cpp_int.hpp>

namespace math_combinatorics
{
    template<typename T>
    void getPermutationsRecursion(
        size_t start
        , const std::vector<T>& current
        , const std::vector<T>& elements
        , const int binSize
        , std::vector<std::vector<T>>& result)
    {
        //for (auto v : current) { cout << v << " "; } cout << endl;
       
        for (auto i = start; i < elements.size(); i++)
        {
            auto val = elements[i];
            std::vector<T> newCurrent { current };
            newCurrent.push_back(val);

            if (newCurrent.size() < binSize)
            {
                getPermutationsRecursion(i + 1, newCurrent, elements, binSize, result);
            }
            else
            {
                //for (auto v : newCurrent) { cout << v << " "; } cout << endl;
                result.push_back(newCurrent);
            }
        }
    }

    template<typename T>
    std::vector<std::vector<T>> getPermutations(const std::set<T>& elements, int binSize)
    {
        assert(binSize <= elements.size());

        std::vector<T> elementVec(elements.size());
        std::copy(elements.begin(), elements.end(), elementVec.begin());

        //cout << "getPermutations for: "; for (auto v : elementVec) { cout << v << " "; } cout << endl;

        std::vector<std::vector<T>> result;
        getPermutationsRecursion(0, {}, elementVec, binSize, result);

        return result;
    }

    void getFactorialDigits(long n, std::vector<int>& result);

    template<typename T>
    T getFactorial(long n)
    {
        T result = 1;
        for (long i = 1; i <= n; i++)
        {
            result *= i;
        }
        return result;
    }

    template<typename T>
    void getPandigitalsRecurse(std::vector<T> input, std::vector<T>& current, std::vector<std::vector<T>>& results)
    { 
        for (int i = 0; i < input.size(); i++)
        {   
            std::vector<T> newCurrent{current};
            newCurrent.push_back(input[i]);

            if (input.size() > 1)
            {
                std::vector<T> elements{input};
                elements.erase(elements.begin() + i);
                getPandigitalsRecurse(elements, newCurrent, results);
            }
            else
            {
                results.push_back(newCurrent);
            }
        }
    }

    template<typename T>
    std::vector<std::vector<T>> getPandigitals(const std::vector<T>& input)
    {
        std::vector<std::vector<T>> results;
        std::vector<T> current;
        getPandigitalsRecurse(input, current, results);
        return results;
    }

    template<typename T>
    void getPandigitalsRecurseWithCondition(
        std::vector<T> input
        , std::vector<T>& current
        , std::vector<std::vector<T>>& results
        , std::function<bool(std::vector<T>)> condition)
    { 
        for (int i = 0; i < input.size(); i++)
        {   
            std::vector<T> newCurrent{current};
            newCurrent.push_back(input[i]);

            if (!condition(newCurrent))
            {
                continue;
            }

            if (input.size() > 1)
            {
                std::vector<T> elements{input};
                elements.erase(elements.begin() + i);
                getPandigitalsRecurseWithCondition(elements, newCurrent, results, condition);
            }
            else
            {
                results.push_back(newCurrent);
            }
        }
    }

    template<typename T>
    std::vector<std::vector<T>> getPandigitalsWithCondition(
        const std::vector<T>& input
        , std::function<bool(std::vector<T>)> condition)
    {
        std::vector<std::vector<T>> results;
        std::vector<T> current;
        getPandigitalsRecurseWithCondition(input, current, results, condition);
        return results;
    }

    long long int getBinomialCoefficient(int k, int n);

	template<typename T>
	T getBinomialCoefficientT(int k, int n)
	{
		assert(n >= k);
	
		using namespace boost::multiprecision;

		T fn = getFactorial<T>(n); 
		T fk = (n == k) ? fn : getFactorial<T>(k);
		T fnk = (n - k == n) ? fn : ((n - k == k) ? fk : getFactorial<T>(n-k));

		//std::cout << "fn: " << fn << ", fk: " << fk << ", fnk: " << fnk << std::endl;

		return  fn / (fk * fnk);
	}
}