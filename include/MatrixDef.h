#ifndef _MATRIX_DEF_H_
#define _MATRIX_DEF_H_

#include <Eigen/Core>

using mat4x4 = Eigen::Matrix4f;

#endif // _MATRIX_DEF_H_