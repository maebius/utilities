#ifndef _PROFILER_H_
#define _PROFILER_H_

#include <vector>
#include <map>
#include "SimpleTimer.h"

#ifdef USE_PROFILING

/// VAGC_PROFILE_* -macros are used to define a profiled block. The timing starts when this object is constructed
/// and ends when this object is destroyed.
/// Start every block of code to be timed with "PROFILE( p , a );", where:
/// - "profilerName" is name of the Profiler to be used (string)
/// - "blockIdentifier" is string identifier for the profiled block (string)
#define PROFILE_BLOCK( profilerName , blockIdentifier )	time_helpers::Profiler::ProfiledBlock profile_instance( time_helpers::Profilers::getProfiler(profilerName), blockIdentifier );
/// This must be called once in a frame for each profiler.
/// @note Call only once in a frame! Marks also the start of the next frame.
/// @note Can be used to measure the performance between two time instances if called
/// in sequence (but the actual intended use is for frame-based measurement)
/// @note Also bear in mind that no samples can be open when calling this macro/method
/// (no open PROFILE_BLOCK instances).
#define PROFILER_FRAME_UPDATE( profilerName ) time_helpers::Profilers::getProfiler(profilerName)->update();

#else
#define PROFILE_BLOCK( p , a )
#define PROFILER_FRAME_UPDATE( p ) 
#endif

namespace time_helpers
{
	// forward declare
	class Profiler;

	/** 
	* Container for profilers. Provides getByName -style access and storage for profilers.
	*/
	class Profilers 
	{
	public:
		/// Static singleton access point for profilers.
		static Profilers * getInstance();

		/// Returns a profiler by name, creating it if necessary.
		static Profiler * getProfiler(const char * profilerName);

		/// Dumps info from all profilers to given file
		static void dumpToFile(const char * file);

	private:
		/// Private Copy-constructor - copying this object is not allowed
		Profilers(const Profilers & src);
		/// Private assignment operator - copying this object is not allowed
		Profilers & operator=(const Profilers & src);

		/// Private constructor, use static getProfiler() instead. 
		Profilers();

		static Profilers * m_instance;

		using ProfilersMap = std::map<const char *, Profiler *, std::less<const char *>>;
		ProfilersMap m_profilers;
	};

	/**
	* Profiler class for doing timing analysis for code. Usable for 
	* finding out where the time is spent when running code. Usage:
	*
	* -# Create instance of a Profiler class, you can have many instances (e.g. for different classes).
	* -# For every block of code to be timed, call "Profile( p , a )", where:
	*    - "p" is instance of the Profiler used
	*    - "a" is string identifier for the profiled block
	* -# The block is profiled to the point where the scope for "Profile( p , a )" ends
	* -# To use profiling, define USE_PROFILING in project settings
	*   - If this is not defined, "Profile( p , a )" macro is defined as empty
	* -# Before profiling information can be fetch, you must call update(),
	*    after this data can be fetch by getTextFrame()
	* -# dumpOutputToBuffer() can't be called inside "Profile( p , a )" block
	* -# Example: 
	*<p><pre>
	*static Profiler *g_globalProfiler = new Profiler();
	*void function()
	*{
	*	int variable = 0; // stuff we do not want to profile
	*	variable = variable*variable;
	*
	*	{ // starting scope for Profile - call
	*		Profile(m_profilerInstance, "function() - timing");
	*
	*		// ... add stuff here that is profiled
	*	} // ending scope for Profile - call
	*}
	*int main()
	*{
	*	function();
	*	g_globalProfiler->dumpOutputToBuffer();
	*	char** profileText = g_globalProfiler->getTextFrame();
	*	for (unsigned int = 0; i < g_globalProfiler->getTextCountFrame(); i++)
	*	{
	*		std::cout << profileText[i] << std::endl;
	*	}
	*	return 0;
	*}
	*</pre></p>
	*
	* This class is adapted from book "Game Programming Gems #1", Charles River Media, 2000.
	* Original copywright:
	*<p><pre>
	* Copyright (C) Steve Rabin, 2000. 
	* All rights reserved worldwide.
	*
	* This software is provided "as is" without express or implied
	* warranties. You may freely copy and compile this source into
	* applications you distribute provided that the copyright text
	* below is included in the resulting source code, for example:
	* "Portions Copyright (C) Steve Rabin, 2000"
	*</pre></p>
	*/
	class Profiler
	{
	public:

		/// Info structure for profiling info
		struct AnalysisInformation 
		{
			double averageTimeSeconds;
			double fastestTimeSeconds;
			double slowestTimeSeconds;
			double currentTimeSeconds;
			double currentTimePercent;
			AnalysisInformation();
		};	

		/// Defines profiled block. The timing starts when this object is constructed
		/// and ends when this object is destroyed.
		/// NOTE! Do not use this directly, but the PROFILE_BLOCK( profilerName , blockIdentifier ) macro instead.
		class ProfiledBlock
		{
		public:
			ProfiledBlock(Profiler* profiler, const char * name);
			~ProfiledBlock();

		private:
			/// Profiler used by this ProfileInstance.
			Profiler *m_profiler;
			/// Unique name for this instance.
			const char * m_name;
		};


		typedef std::map<const char *, AnalysisInformation, std::less<const char *>> AnalysisInformationMap;

		Profiler(const char * name);
		~Profiler();

		/// Sets the amount of indentation (number of white spaces) for nested samples.
		void setNestedSampleIndentation(unsigned int indentation);

		/// Returns analysis information for given profiler sampling point
		const AnalysisInformation & getInfo(const char * name) const;

		/// Call once per frame to update profiling information 
		/// @note 
		/// Needs to be called prior getting profiling information. Must not be
		/// called when samples are open.
		void update();

		/// Formats current profiler state output to multiline readable char array.
		/// @return number of characters written
		size_t writeFormattedOutput(char * outputBuffer
			, size_t outputBufferLen
			, size_t* numberOfLines_out=nullptr) const;

		/// Formats the history buffer  output to multiline readable char array.
		/// @return number of characters written
		size_t writeFormattedHistoryOutput(char * outputBuffer
			, size_t outputBufferLen
			, size_t* numberOfLines_out=nullptr) const;

		/// Dumps profiler info to given text file
		void dumpInfoToFile(const char * file, bool append = false) const;
				
		/// Returns the FPS for the last frame
		float getCurrentFps() const;

		/// Returns the duration of the last full frame
		double getLastFrameDuration() const;

	protected:
		/// ProfileInstance is a friend so it can access our protected members.
		friend class ProfiledBlock;

		/// Starts profiling of a block, called by ProfileInstance.
		void begin( const char * name );
		/// Ends profiling of a block, called by ProfileInstance.
		void end( const char * name );
			
	protected:
		/// A struct to hold one profiler sample.
		struct ProfilerSample 
		{
			static const int NAME_MAX_LENGTH = 128;
			///Whether this data is valid
			bool valid;		
			///Number of times ProfilerBegin called
			unsigned int profilerInstances; 
			///Number of times ProfilerBegin w/o ProfilerEnd
			int openProfilers;				
			///Name of sample
			char name[NAME_MAX_LENGTH];
			///The current open Profiler start time
			double startTime;	
			///All samples this frame added together
			double accumulatorTime;			
			///Time taken by all children
			double childrenSampleTime;		
			///Number of Profiler parents
			unsigned int numberOfParents;       
		};

		/// A one history sample for profiler.
		struct ProfilerSampleHistory
		{
			static const int NAME_MAX_LENGTH = 128;
			/// Whether the data is valid
			bool valid;        
			/// Number of times called.
			long timesCalled;  
			/// Name of the sample
			char name[NAME_MAX_LENGTH];
			/// Average time per frame
			double aveTime;         
			/// Minimum time per frame
			double minTime;         
			/// Maximum time per frame
			double maxTime;         
			/// Average time per frame (percentage)
			double avePct;         
			/// Minimum time per frame (percentage)
			double minPct;         
			/// Maximum time per frame (percentage)
			double maxPct;   
			///Number of Profiler parents
			unsigned int numberOfParents;       
		};

	private:
		/// Private Copy-constructor - copying this object is not allowed
		Profiler(const Profiler & src);
		/// Private assignment operator - copying this object is not allowed
		Profiler & operator=(const Profiler & src);

	private:	

		/// Marks the end time of the current frame
		void markFrameEnd(double currentTimeSeconds);  
		/// Initialize profiling class, called on object construction.
		void initialize( void );
		/// Uninitialize stuff, called on destruction.
		void unInitialize( void );  
		/// Stores sample information in history buffer.
		void storeInHistory( const char * name, int numberOfParents, double sample, double percent );
		/// Retrieves sample information for given id from history buffer.
		void getFromHistory( const char * name, double* ave, double* min, double* max );
		/// Calculates FPS for last frame.
		void updateFrameRateCounter(double currentFrameTimeSeconds);
		
		/// Gets current time since object init.
		double getExactTime();

		/// How many samples we can have open in one moment in one Profiler.
		static const int NUM_SAMPLES  = 10000;
		/// How many different samples we can have during application time in total.
		static const int NUM_SAMPLES_HISTORY = 80;
		/// What is the maximum line length for one sample 
		/// (actual sample name is less, do not exceed 100 chars!)
		static const int TEXT_LINE_CHAR_MAX = 200;

		/// Amount of indentation (number of white spaces) for nested samples.
		static unsigned int sm_nestedSampleIndentation;

		/// System start time (since reboot)
		double m_systemStartTime;

		/// Object init time.
		double m_profilingStartTime;
		/// Duration of the last full measured frame in seconds
		double m_lastFrameDuration;

		/// All active samples.
		ProfilerSample m_samples[NUM_SAMPLES];
		/// All samples in history buffer.
		ProfilerSampleHistory m_history[NUM_SAMPLES_HISTORY];

		/// Our timer object, doing all the timing related operations.
		SimpleTimer<std::chrono::milliseconds> m_timer;

		/// When did our frame start.
		double m_frameStartTime;
		/// When did our frame stop.
		double m_frameEndTime;

		double m_lastTimeTick;

		/// Do we show profiling information in percentages or in absolute time.
		bool m_percentages;

		/// The amount of frames that are used to calculate average of the FPS number
		static const int FPS_NUM_SAMPLES = 10;

		/// How many frames do we use for calculating the FPS.
		int m_fpsSampleCount;
		/// Frame start time ring buffer for FPS calculations. FPS is a simply (end-start/frames).
		double m_fpsFrameStartTimes[FPS_NUM_SAMPLES];
		/// Where can we insert a new fps start time sample in the m_fpsStartTimeSeconds buffer
		size_t m_fpsOldestSampleIndex;
			
		/// Current FPS.
		float m_fps;

		/// Analysis information
		AnalysisInformationMap m_analysisInformation;
		
		/// The name of this profiler
		const char * m_name;
	};
} 

#endif // _PROFILER_H_
