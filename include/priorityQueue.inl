#pragma once

#include "heapSort.h"
#include "sortingException.h"
#include "PrintHelpers.h"

namespace sort_utilities
{
	template<class T>
	PriorityQueue<T>::PriorityQueue(TVec* input, std::function<bool(const T, const T)> sortCriteria)
		: _data{input}
		, _sortCriteria{sortCriteria}
		, _ownsData{false}
	{
		if (!_data)
		{
			// no data provided, so act as an owner for it
			_data = new TVec;
			_ownsData = true;
		} 

		// just build a heap inplace for the input
		buildHeap<T>(_data, _sortCriteria);
	}

	template<class T>
	PriorityQueue<T>::~PriorityQueue()
	{
		if (_ownsData)
		{
			delete _data;
		}
	}

	template<class T>
	unsigned int PriorityQueue<T>::size() const
	{
		return _data->size();
	}

	template<class T>
	bool PriorityQueue<T>::empty() const
	{
		return _data->empty();
	}

	template<class T>
	void PriorityQueue<T>::clear()
	{
		_data->clear();
	}

	template<class T>
	T& PriorityQueue<T>::top() const
	{
		// top is the root as the heap property is maintained
		return (*_data)[0];
	}
	
	template<class T>
	T PriorityQueue<T>::pop()
	{
		if (size() == 0)
		{
			throw new SortingException("no elements in queue!");
		}

		T top = (*_data)[0];

		// now, make the heap one smaller, and take the last element
		// and put it in the root
		auto newSize = _data->size() - 1;
		(*_data)[0] = (*_data)[newSize];

		// ... then call heapify for the root to recapture the heap property
		heapify<T>(_data, _sortCriteria, newSize, 0);			

		// cleanup of actually getting rid of the element we extracted
		_data->pop_back();

		return top;
	}

	template<class T>
	void PriorityQueue<T>::push_back(T value)
	{
		// just store as last element in queue
		_data->push_back(value);
		// this will push the value to correct node in our heap
		increaseKey(size() - 1);
	}

	template<class T>
	void PriorityQueue<T>::erase(TVecIte value)
	{
		_data->erase(value);
	}

	template<class T>
	void PriorityQueue<T>::increaseKey(int index)
	{
		if (size() == 0)
		{
			throw new SortingException("no elements in queue!");
		}

		auto element = (*_data)[index];
		auto parentIndex = index / 2;
		auto parent = (*_data)[parentIndex];

		// as long as the heap property is not satisfied with node and its parent,
		// exchange their places and reprocess then the parent
		
		while (index > 0 && _sortCriteria(parent, element))
		{
			(*_data)[index] = parent;
			(*_data)[parentIndex] = element;

			index = parentIndex;
			parentIndex = index / 2;

			element = (*_data)[index];
			parent = (*_data)[parentIndex];
		}
	}
};
