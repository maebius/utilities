#include <vector>
#include <iostream>
#include <algorithm>

#include "Debug.h"

namespace print_helpers
{
	template<typename T>
	void printVector(const std::vector<T> & input, const std::string & format)
	{
		auto print = [&format](T t) 
		{ 
			INFO_PRINTF(format.c_str(), t);
		};
		std::for_each(input.begin(), input.end(), print);
	}

	template<typename T> 
	void printQueueByPopping(T& q, const std::string & format)
	{
		while (!q.empty()) 
		{
			INFO_PRINTF(format.c_str(), q.top());
			q.pop();
		}
	}
}