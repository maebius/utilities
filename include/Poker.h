#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <map>

namespace poker
{
	enum class Suit : unsigned char
	{
		Heart		= 'h'
		, Spade		= 's'
		, Diamond	= 'd'
		, Club		= 'c'
	};

	enum class HandValue : unsigned short
	{
		HighCard		= 1
		, Pair
		, TwoPairs
		, ThreeOfAKind
		, Straight		
		, Flush
		, FullHouse
		, FourOfAKind
		, StraightFlush
	};

	struct Card
	{
		Suit	suit;
		short	value; /// [2,14], where 11 = Jack, 14 = Ace

		friend std::ostream &operator<<(std::ostream&, const Card&);
	};

	class Hand
	{
		public:
			Hand(const std::string);


			friend bool operator ==(const Hand&, const Hand&);
			friend bool operator !=(const Hand&, const Hand&);
			friend bool operator >=(const Hand&, const Hand&);
			friend bool operator <=(const Hand&, const Hand&);
			friend bool operator >(const Hand&, const Hand&);
			friend bool operator <(const Hand&, const Hand&);

			friend std::ostream &operator<<(std::ostream&, const Hand&);

		private:
			void parse(const std::string);

			void evaluate();
			void evaluateCategory();
			void evaluateInsideCategory();
			void reorderHand();
	
			void moveSameAsToPosition(int, int, short);
			void moveDifferentThanToPosition(int, int, short);

			std::vector<Card>	_cards;
			HandValue			_highLevelValue;
			int					_detailedValue;

			std::vector<short>	_sameSequences; 
			std::vector<short>	_sameSequenceValues; 

	};
}

