#ifndef _OPENGL_HELPERS_H_
#define _OPENGL_HELPERS_H_

#include <cstdlib>

#include "OpenGLInclude.h"
#include "VectorDef.h"

namespace graph_utilities 
{
	struct Graph;
}

namespace opengl_helpers
{
	void drawGraph(graph_utilities::Graph* graph);
	
	void drawBox(vec3 origin, vec3 halfDimension
		, GLfloat r, GLfloat g, GLfloat b, float lineWidth);
}

#endif // _OPENGL_HELPERS_H_
