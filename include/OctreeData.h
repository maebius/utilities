#ifndef _OctreePoint_H
#define _OctreePoint_H

#include "VectorDef.h"
#include "Triangle.h"

class Octree;

enum class DataMode
{
	Vertex, Triangle
};

/*! \class OctreeData
	\brief Class for holding Octree nodes actual data

	Class to hold data for an Octree cell. The data can basically be
	anything, but currently it is just a (vertex) position.
	The data should be actually pointer to the geometry data to the structure
	occupying the space, such as list of triangles/polygons in space.
*/
class OctreeData
{
public:
	virtual vec3* getPosition() = 0;
	virtual int getPointCount() = 0;
	virtual vec3* getPoint(unsigned int index) = 0;
	virtual vec3 getClosest(const vec3& point) = 0;
};

class OctreeDataVertex : public OctreeData
{
public:
	vec3* getPosition() override { return _data; }
	int getPointCount() override { return 1; }
	vec3* getPoint(unsigned int index) override
	{
		assert(index == 0);
		return _data;
	} 
	vec3 getClosest(const vec3& point) override
	{
		// the only data here is obviously the closest one
		return *_data;
	}

	inline const vec3* getData() const { return _data; }
	inline void setData(vec3* p) { _data = p; }

private:
	vec3* _data; //! Our geometric 'data' that is tied to the Octree node where this class instance is found
};

class OctreeDataTriangle : public OctreeData
{
public:
	vec3* getPosition() override { return &(_data->centroid); }

	int getPointCount() override { return 3; }
	vec3* getPoint(unsigned int index) override
	{
		assert(index < 3);
		return &(_data->p[index]);
	}

	vec3 getClosest(const vec3& point) override
	{
		return triangle_helpers::getClosestPoint(_data, point);
	}

	inline const Triangle* getData() const { return _data; }
	inline void setData(Triangle* t) { _data = t; }

private:
	Triangle* _data; //! Our geometric 'data' that is tied to the Octree node where this class instance is found
};


#endif
