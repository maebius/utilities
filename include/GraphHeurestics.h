#pragma once

namespace graph_utilities
{
	struct Graph;
	struct Node;
}

namespace graph_heurestics
{
	float euclideanDistanceSquared(
		const graph_utilities::Graph* graph
		, const graph_utilities::Node* node
		, const graph_utilities::Node* goal);
}
