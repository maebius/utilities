#ifndef Octree_H
#define Octree_H

#include <cstddef>
#include <vector>
#include "OctreeData.h"

class OctreeData; //! Forward declaration for the data class that occupies the Octree nodes

/*! \class Octree
	\brief Class for simple Octree spatial 3D hierarchy

	Class for spatial data structure, that divides the space in quadrants.
	Traditianally the space is only divided in necessary, e.g. 1-8 subnodes,
	but this simple implementation either is a leaf (no division), or subdivided
	so that it has exactly 8 children.

	When we add data to this datastructure, we check where in one of the quadrants
	the data would go, and then recursively let that quadrant handle the addition.
	If the Octree is not divided already, we create the subnodes first.
*/ 
class Octree 
{
public:
	/*!
		Constructor for the Octree. Note that this is used recursively inside an Octree hierarchy.

		\param parent Parent node for the node, if nullptr, then this is the root.
		\param origin The geometrical center for the input vertices.
		\param halfDimension Half of the bounding volume dimensions for the given input vertices.
	*/
	Octree(Octree* parent, const vec3& origin, const vec3& halfDimension);
	~Octree();

	static bool checkIfEnclosingFully(Octree* root, OctreeData *data);

	bool containsPoint(const vec3* point);

	/*!
		Given the bounding box, gives all the Data that is stored in this Octree in a list.

		\param bbMin The minimum corner for the bounding volume used for query
		\param bbMax The maximum corner for the bounding volume used for query
		\param result The result vector that will contain all data found with the query
	*/
	void getPointsInsideBox(const vec3& bbMin, const vec3& bbMax, std::vector<Octree*>& result);
	
	/*!
		For given point, gives the leaf Octree node that encloses the point. Returns nullptr if point outside 
		the whole bounding box.

		\param point The point that we try to locate in our Octree.
	*/
	Octree* getFirstChildForPoint(const vec3* point);
	Octree* getLeafForPoint(const vec3* point);
	Octree* getFirstEnclosingNodeForPoint(const vec3* point);

	OctreeData* getData(); //! Get this nodes assocaited data. Is nullptr if this is a leaf.
	int getFullyContainedDataCount(); //! Number of fully contained data elements.
	OctreeData* getFullyContainedData(unsigned int index); //! Get the fully contained data element with given index.
	Octree* getFirstWithData(); //! Take recursively the first node from children that is not nullptr.
	Octree* getParent(); //! Get this nodes parent, is nullptr if this is root.
	std::vector<Octree*> getChildren(); //! Gets the non-null children

	const vec3 getOrigin(); //! Get the center of this node.
	const vec3 getHalfDimension(); //! Get the half w/h/d of this nodes bounding box.

	bool isLeafNode() const; //! Is this node leaf, e.g. has no children?

	void insert(OctreeData*); //! Add data to this structure. Will subdivide if necessary.

private:	
	int getOctantContainingPoint(const vec3*) const; //! Get the octant where given point lies, used internally.

	vec3 _origin;         //! Center of this node
	vec3 _halfDimension;  //! Half the width/height/depth of this node

	Octree *_parent; //! The parent for this node, nullptr if this itself is root.
	
	Octree* _children[8]; //! Pointers to child octants, if this is a leaf node these are all nullptr.
	OctreeData *_data;   //! This node's actual data.

	std::vector<OctreeData*> _dataFullyContainedThisLevel; //! The data that fits entirely in this node, but might span across several children.
	Octree* _enclosingCell; //! Cache for the cell that fully contains the result after getFirstEnclosingNodeForPoint() query.
};

#endif
