#pragma once

#include <string>

namespace sort_utilities
{
	class SortingException
	{
	public:
		SortingException(std::string error);
	};
};