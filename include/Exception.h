#ifndef _EXCEPTION_H_
#define _EXCEPTION_H_

#ifndef EXCEPTIONS_DISABLED
#include <exception>
#include <string>
#endif

// Preprocessor magic to concatenate __FILE__ and __LINE__ into single LOCATION string macro
// The following macros are based on article by Curtis Krauskopf,
// http://www.decompile.com/cpp/faq/file_and_line_error_string.htm
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

#if !defined(CONFIG_SHIPPING) || defined(FORCE_SHOW_LOCATION)
#define LOCATION __FILE__ "(" TOSTRING(__LINE__) ")"
#else
#define LOCATION ""
#endif 

#if defined(NDEBUG) && !defined(USE_CUSTOM_ASSERT)

// CUSTOM_ASSERTs disabled 
#define CUSTOM_ASSERT(test, message, ...) (void)0
#define CUSTOM_ASSERT1(test) (void)0

// assert disabled
#ifndef assert
#define assert(test) ((void)0)
#endif //assert

#else
// CUSTOM_ASSERTs and other asserts are enabled --->

// CUSTOM_ASSERTs - our own assert macro.
// Prefer one with a clear message, like VAGC_ASSERT(returncode == CELL_OK, "Job::initialize() - Job size exceeded!");
//
// The macro below is identical to the following code, but seems to avoid "conditional expression is constant" warnings
// with the Visual C++ compiler. Original code from the Microsoft Visual C++ assert.h header.
// if (!(test)) VAGC::Exception::throwError(LOCATION, "VAGC_ASSERT (" #test ") failed, message: " message, ##__VA_ARGS__)
#define CUSTOM_ASSERT(test, message, ...) (void)( (!!(test)) || (Exception::throwError(LOCATION, "CUSTOM_ASSERT ( " #test " ) failed, message: " message, ##__VA_ARGS__),0))
#define CUSTOM_ASSERT1(test) (void)( (!!(test)) || (Exception::throwError(LOCATION, "CUSTOM_ASSERT ( " #test " ) failed."),0))

// Provide an assert() substitute. NOTICE! This is not 100% reliable, because also 
// standard Microsoft assert.h undefines and redefines assert(). --> USING assert() 
// is NOT recommended!! Use CUSTOM_ASSERT() or CUSTOM_ASSERT1() instead.
#ifdef assert
#undef assert
#endif //assert
#define assert(test) (void)( (!!(test)) || (Exception::throwError(LOCATION, "assert( " #test " ) failed."),0))

// NO_CUSTOM_ASSERT_DEBUG
#ifdef NO_CUSTOM_ASSERT_DEBUG
#define CUSTOM_ASSERT_DEBUG(test, message, ...) (void)0
#else
#define CUSTOM_ASSERT_DEBUG(test, message, ...) if (!(test)) Exception::throwError(LOCATION, "CUSTOM_ASSERT_DEBUG (" #test ") failed, message: " message, ##__VA_ARGS__)
#endif

#endif // defined NDEBUG


/// Halts the execution of a program in an error condition.
//lint -function(abort(0), VAGC::error_freeze)
void error_freeze();
/// Raises a software breakpoint, should stop debugger when running.
void debug_break();


/**
* Exception base class for all Custom exceptions, and
* static utility methods for throwing exceptions.
*
* Currently throwError() throws std::strings as exceptions, but this might change
* in the future.
*
* Throwing exceptions should be done through the throwError(..) static
* utility methods. This allows us some flexibility to change the exception types
* as needed.
*
* Some usage examples:
* <p><pre>
  Exception::throwError(LOCATION, "MOFile::MOFile() - Constructed with invalid null pointer as filename");
* </pre></p>
*/
class Exception 
#ifndef CUSTOM_EXCEPTIONS_DISABLED
	: public std::exception
#endif

{
public:
	static const int EXCEPTION_MESSAGE_MAX_LENGTH_CHARS = 512;

public:

	/**
	* Set behavior of error throwing methods.
	* @param enableDebugBreak Set to true to generate a software breakpoint when one of the
	* Exception::throwError* methods get called. Will stop the execution if debugger is running.
	* Default is true.
	* @param freezeOnError Set to true to enter infinite loop instead of aborting, when
	* one of the Exception::throwError* methods get called.
	**/
	static void setActionOnError(bool enableDebugBreak, bool freezeOnError);

	// -------- Static methods to generate errors --------

	/**
	* Convenience method to generate an error (throw an exception).
	* @param location The location where the error occurred, use LOCATION preprocessor macro.
	* @param format Information about the cause of this exception in printf-style and you should format your
	* messages like "Class::method() - Message .." or "file.cpp::function() - Message ..".
	* Example:
	* <p><pre>
		Exception::throwError(LOCATION, "MOFile::MOFile() - Could not open file %s", fileName);
	</pre></p>
	*/
	// this func does not return:
	//lint -function(abort(0), Exception::throwError, VAGC::Exception::throwError)
	// 2nd param acts like printf:
	//lint -printf(2, Exception::throwError, VAGC::Exception::throwError)
	static void throwError(const char* location, const char * format, ...);

	/**
	* Convenience method to generate an error (throw an exception).
	* The actual values of the file and line arguments
	* come from the C++ preprocessor macros __FILE__ and __LINE__, which always
	* refer to the current file and line number.
	* @param file The source file where the error occurred, use __FILE__ preprocessor macro.
	* @param line The source file where the error occurred, use __LINE__ preprocessor macro.
	* @param format Information about the cause of this exception in printf-style and you should format your
	* messages like "Class::method() - Message .." or "file.cpp::function() - Message ..".
	*/
	static void throwError(const char * file, int line, const char * format, ...);

	/**
	* Convenience method to generate an error (throw an exception) if given condition
	* is true.
	* @param condition Generate an error if this condition is true.
	* @param location The location where the error occurred, use LOCATION preprocessor macro.
	* @param message Information about the cause of this exception in printf-style and you should format your
	* messages like "Class::method() - Message .." or "file.cpp::function() - Message ..".
	*
	* You can also use this via the VAGC_ASSERT() macro with inverted condition.
	*/
	// this func behaves like assert
	//lint -function( __assert(1), Exception::throwErrorIf(1))
	// 3rd param acts like printf:
	//lint -printf(3, Exception::throwErrorIf)
	static void throwErrorIf(bool condition, const char* location, const char * message, ...);

#ifndef CUSTOM_EXCEPTIONS_DISABLED
public:
	Exception() /*throw()*/;
	virtual ~Exception() /*throw()*/;

	/**
	* Creates an exception with an error message.
	* @param errormsg Information about the cause of this exception. You should format your
	* messages like "Class::method() - Message .." or "file.cpp::function() - Message ..".
	*/
	Exception(const std::string & errormsg);

	/**
	* Creates an exception with a formatted error message.
	* @param formatstr Information about the cause of this exception in printf-style and you should format your
	* messages like "Class::method() - Message .." or "file.cpp::function() - Message ..".
	*/
	Exception(const char *formatstr, ...);


	/**
	* Returns the exception message, including the source of the exception and
	* the cause as a String.
	*/
	virtual std::string str() const /*throw()*/;

	/**
	* Returns the exception message, including the source of the exception and
	* the cause as c string. Implements std::exception::what().
	*/
	virtual const char* what() const throw();

protected:
	std::string error;

#else  // Exception class without exceptions enabled

public:	
	/// Sets an error handler - will be called if Exception::throwError() 
	/// has been invoked. Set an error handler if you want to do additional 
	/// error handling, like logging the error somewhere..
	static void setErrorHandler(void (*error_callback_func)(const char *location, const char * message));

#endif // CUSTOM_EXCEPTIONS_DISABLED

};

#endif // _EXCEPTION_H_
