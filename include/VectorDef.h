#ifndef _VECTOR_DEF_H_
#define _VECTOR_DEF_H_

#include <Eigen/Core>

using vec4 = Eigen::Vector4f;
using vec3 = Eigen::Vector3f;
using vec2 = Eigen::Vector2f;

using mat4x4 = Eigen::Matrix4f;

vec3 vec4_to_vec3(const vec4 & src);
vec4 vec3_to_vec4(const vec3 & src, float fourth);

#endif // _VECTOR_DEF_H_