#ifndef MathHour_MathHelpers_h
#define MathHour_MathHelpers_h

//#include "cocos2d.h"
#include <vector>

//#include "MHTerm.h"
//#include "MHNumber.h"
#include "VectorDef.h"

namespace MathHelpers
{
    //using namespace cocos2d;
    
	const float NearZeroLimit = 0.00001f;

	std::vector<int> getRandomOrder(int);

	long getReverse(const long&);
	bool isPalindrome(const long&);

    bool calculateLineCoefficients(const vec2&, const vec2&, float&, float&);
	int getRandom(int, int);
	bool isInteger(double);
	bool isSqrtInteger(long long, long long&); 
	
	bool isNearZero(float, float = NearZeroLimit);

	bool findGreatestCommonDivisor(int, int, int*);

	bool isTokenOk(const std::string &token);
	//bool isEquationValid(const Vector<MHTerm*> &sortedTerms);

	// shunting yard stuff
	int op_preced(const char c);
	bool op_left_assoc(const char c);
	unsigned int op_arg_count(const char c);

	bool shunting_yard(const char *input, char *output);
	bool execution_order(const char* input, std::vector<double> *dataArray);
}

#endif // MathHour_MathHelpers_h